package ch.heig.soo_gui.network

enum class MessageType(val byteCode: Byte) {
    GET_LIST(0x04),
    SEND(0x05),
    SELECT(0x06),
    POST(0x07),
    UNKNOWN(0x00);

//    companion object {
//        fun from(value: Byte): MessageType {
//            return when(value.toInt()){
//                1 -> GET_LIST
//                2 -> SEND
//                4 -> SELECT
//                8 -> POST
//                else -> UNKNOWN
//            }
//        }
//    }
}