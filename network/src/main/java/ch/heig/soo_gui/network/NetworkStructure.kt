package ch.heig.soo_gui.network

data class NetworkStructure(val type: MessageType, val spid: String?, val payload: String?, val slotID: Int?)
