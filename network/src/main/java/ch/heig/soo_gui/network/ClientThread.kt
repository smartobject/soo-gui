@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.network

import android.util.Log
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.Exception

data class ClientThread(val networkHandler: NetworkHandler, val onExceptionHandler: (e: Exception)-> Unit) : Thread() {
    private val TAG = "ClientThread"
    private val running = AtomicBoolean(true)

    override fun interrupt() {
        super.interrupt()
        running.set(false)
    }

    override fun run() {
        while (running.get()) {
            try {
                networkHandler.receive()
            } catch (e: Exception) {
                Log.e(TAG, "Cannot received data anymore.", e)
                running.set(false)
                onExceptionHandler(e)
            }
        }
    }

    fun send(messageType: MessageType, spid: String?, payload: String?, slotID: Int?) {
        if (running.get()) {
            try {
                networkHandler.send(
                    NetworkStructure(
                        type = messageType,
                        spid = spid,
                        payload = payload,
                        slotID = slotID
                    )
                )
            } catch (e: Exception) {
                Log.e(TAG, "Cannot send data anymore.", e)
                running.set(false)
                onExceptionHandler(e)
            }
        }
    }

    fun send_plain(payload: ByteArray, size: Int, slotID: Int) {

        if (running.get()) {
            try {
                networkHandler.send_plain(payload, size, slotID)
            } catch (e: Exception) {
                Log.e(TAG, "Cannot send data anymore.", e)
                running.set(false)
                onExceptionHandler(e)
            }
        }
    }
}
