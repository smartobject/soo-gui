package ch.heig.soo_gui.network

import android.util.Log

class ByteArrayHelper {
    companion object {
        fun convertIntToByteArray(value: Int): ByteArray {
            val result = ByteArray(Int.SIZE_BYTES)

            val mask = 0x00FF
            var n = value

            for (i in result.indices) {
                result[i] = n.and(mask).toByte()
                n = n.shr(Byte.SIZE_BITS)
            }

            return result
        }

        fun String.decodeHex(numberBytes: Int = 0): ByteArray {
            return "0".repeat(
                kotlin.math.max(
                    0,
                    (numberBytes * 2) - this.length
                )
            ) // prepend a number of '0' to complete the array
                .plus(this)
                .reversed() // so we chunk from the end
                .chunked(2)
                .map {
                    it.reversed()       // because the values are reversed
                        .toInt(16)
                        .toByte()
                }
                .reversed() // so we put in the right order
                .toByteArray()
        }
    }
}