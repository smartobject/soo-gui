@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.network

import android.os.Handler
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.util.concurrent.atomic.AtomicReference
import kotlin.experimental.and


data class NetworkPlainHandler(
    override val inputStream: InputStream,
    override val outputStream: OutputStream,
    override var inputHandler: AtomicReference<Handler>
) : NetworkHandler {
    private val TAG = "NetworkPlainHandler"

    override fun receive() {
        val bufferReadOutputStream = ByteArrayOutputStream()
        val bufferTotal = ByteArrayOutputStream()
        var endMessage: Boolean
        var numTotalBytes = 0
        var numBytes: Int

        // get all the block of messages
        val offset = MESSAGE_TYPE_SIZE
        do {
            val buffer = ByteArray(BLOCK_SIZE)
            numBytes = inputStream.read(buffer)
            Log.d(TAG, "Number of bytes: $numBytes")
            numTotalBytes += numBytes

            bufferReadOutputStream.write(buffer, offset, numBytes - offset)
            endMessage = (buffer[0] and CONTINUE_MESSAGE_FLAG) == 0b0000_0000.toByte()
        } while (numBytes != -1 && !endMessage)

        // send the message once all is received
        if (endMessage) {
            Log.d(TAG, "Number of total bytes: $numTotalBytes")
            bufferTotal.write(bufferReadOutputStream.toByteArray())

            // Send the obtained bytes to the UI Activity
            inputHandler.get().obtainMessage(
                MessageType.SEND.ordinal,
                numTotalBytes,
                -1,
                bufferTotal.toByteArray()
            ).sendToTarget()
        }
    }

    override fun send(networkStructure: NetworkStructure) {
        val fullMessage = ByteArrayOutputStream()
        //fullMessage.write(networkStructure.type.byteCode.toInt())
        if (!networkStructure.payload.isNullOrBlank()) {
            fullMessage.write(networkStructure.payload.toByteArray())
        }

        outputStream.write(fullMessage.toByteArray())
        outputStream.flush()
    }

    override fun send_plain(payload: ByteArray, size: Int, slotID: Int?) {
        outputStream.write(payload, 0, size)
    }
}