package ch.heig.soo_gui.network

import android.os.Handler
import android.util.Log
import ch.heig.soo_gui.network.ByteArrayHelper.Companion.decodeHex
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.util.concurrent.atomic.AtomicReference
import kotlin.experimental.and

data class NetworkVUIHandler(
    override val inputStream: InputStream,
    override val outputStream: OutputStream,
    override var inputHandler: AtomicReference<Handler>
) : NetworkHandler {
    private val TAG = "NetworkVUIHandler"

    override fun receive() {
        val bufferPayloadReadOutputStream = ByteArrayOutputStream()
        val bufferTotal = ByteArrayOutputStream()
        var endMessage: Boolean
        var numTotalBytes = 0
        var numBytes: Int
//        var messageTypeByte: Byte = 0
        var spidTypeBytes: ByteArray? = null

        // get all the block of messages
        val offset = HEADER_SIZE
        do {
            val buffer = ByteArray(BLOCK_SIZE)
            numBytes = inputStream.read(buffer)
            Log.d(TAG, "Number of bytes: $numBytes")
            numTotalBytes += numBytes

            // depend on the type
            endMessage = (buffer[0] and CONTINUE_MESSAGE_FLAG) == 0b0000_0000.toByte()

            // get the message type
//            if (endMessage) {
//                messageTypeByte = buffer[0]
//            }

            if (spidTypeBytes == null) {
                spidTypeBytes = buffer.copyOfRange(MESSAGE_TYPE_SIZE, SPID_SIZE)
            }

            // write the payload
            bufferPayloadReadOutputStream.write(buffer, offset, numBytes - offset)
        } while (numBytes != -1 && !endMessage)

        // send the message once all is received
        if (endMessage) {
            Log.d(TAG, "Number of total bytes: $numTotalBytes")
            bufferTotal.write(bufferPayloadReadOutputStream.toByteArray())

            // Send the obtained bytes to the UI Activity
            inputHandler.get().obtainMessage(
                MessageType.SEND.ordinal,
                numTotalBytes,
                -1,
                bufferTotal.toByteArray()
            ).sendToTarget()
        }
    }


    override fun send(networkStructure: NetworkStructure) {
        val fullMessage = ByteArrayOutputStream()


        // write the message type
        fullMessage.write(networkStructure.type.byteCode.toInt())

        // write the spid
        // the spid is considered to be an hex string
        // e.g. : 10203 => [00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 01, 02, 03]
        /*val spidByteArray = networkStructure.spid?.decodeHex(SPID_SIZE) ?: ByteArray(SPID_SIZE)
        fullMessage.write(spidByteArray)*/


        if (!networkStructure.payload.isNullOrBlank()) {
            fullMessage.write(networkStructure.payload.toByteArray())
        }


        send_plain(fullMessage.toByteArray(), networkStructure.type.byteCode.toInt(), networkStructure.slotID)
    }

    override fun send_plain(payload: ByteArray, type: Int, slotID: Int?) {


        var head = Header()
        head.type = 0x1
        head.vuihandlerType = type
        head.slotID = slotID!!

        var max_playload_size = 950 - HEADER_SIZE
        var byte_to_send = payload.size

        while (byte_to_send != 0) {
            var chunk_size = kotlin.math.min(byte_to_send, max_playload_size)

            val finalBuf = ByteArray(HEADER_SIZE + chunk_size)
            System.arraycopy(head.serialize(), 0, finalBuf, 0, HEADER_SIZE)
            System.arraycopy(payload, payload.size - byte_to_send, finalBuf, HEADER_SIZE, chunk_size)


            outputStream.write(finalBuf, 0, finalBuf.size)
            outputStream.flush()
            byte_to_send -= chunk_size
        }
    }
}