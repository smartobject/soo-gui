package ch.heig.soo_gui.network

/* HEADER */
const val ME_DATA_PKT = 0x2
const val ME_SIZE_PKT = 0x3

const val CONSISTENCY_SIMPLE = 0x0
const val CONSISTENCY_EXTENDED = 0x1

const val HEADER_SIZE = 33

const val BLOCK_SIZE = 1008
const val MESSAGE_TYPE_SIZE = 1
const val SPID_SIZE = 16
//const val PAYLOAD_SIZE = 960
const val PAYLOAD_SIZE = BLOCK_SIZE - (MESSAGE_TYPE_SIZE + SPID_SIZE)

const val LAST_PKT_TRANSID = 0x01000000


const val CONTINUE_MESSAGE_FLAG : Byte = 0b1000_0000.toByte()
/* END HEADER */