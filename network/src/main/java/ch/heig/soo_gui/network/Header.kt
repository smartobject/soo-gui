package ch.heig.soo_gui.network

import ch.heig.soo_gui.network.ByteArrayHelper.Companion.convertIntToByteArray

class Header {
    var transID = 0
    var packetID = 0
    var nbPackets = 0
    var payloadSize = 0
    var vuihandlerType = 0
    var consistency = 0
    var type = 0
    var slotID = 0


    fun serialize(): ByteArray {
        val serializedHeader = ByteArray(HEADER_SIZE)

        serializedHeader[0] = type.toByte()

        val transIDInByte = convertIntToByteArray(transID)
        serializedHeader[8] = transIDInByte[0]
        serializedHeader[9] = transIDInByte[1]
        serializedHeader[10] = transIDInByte[2]
        serializedHeader[11] = transIDInByte[3]

        serializedHeader[12] = consistency.toByte()

        var inByte: ByteArray = convertIntToByteArray(packetID)
        serializedHeader[16] = inByte[0]
        serializedHeader[17] = inByte[1]
        serializedHeader[18] = inByte[2]
        serializedHeader[19] = inByte[3]

        inByte = convertIntToByteArray(nbPackets)
        serializedHeader[20] = inByte[0]
        serializedHeader[21] = inByte[1]
        serializedHeader[22] = inByte[2]
        serializedHeader[23] = inByte[3]

        inByte = convertIntToByteArray(payloadSize)
        serializedHeader[24] = inByte[0]
        serializedHeader[25] = inByte[1]

        inByte = convertIntToByteArray(slotID)
        serializedHeader[28] = inByte[0]
        serializedHeader[29] = inByte[1]
        serializedHeader[30] = inByte[2]
        serializedHeader[31] = inByte[3]

        /* VUIHANDLER header */
        serializedHeader[32] = vuihandlerType.toByte()

        return serializedHeader
    }
}