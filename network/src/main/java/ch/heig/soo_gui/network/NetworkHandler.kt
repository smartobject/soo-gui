package ch.heig.soo_gui.network

import android.os.Handler
import java.io.InputStream
import java.io.OutputStream
import java.util.concurrent.atomic.AtomicReference

interface NetworkHandler {
    val inputStream: InputStream
    val outputStream: OutputStream
    var inputHandler: AtomicReference<Handler>

    fun receive()
    fun send(networkStructure: NetworkStructure)
    fun send_plain(payload: ByteArray, size: Int, slotID: Int?)
}