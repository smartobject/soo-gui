package ch.heig.soo_gui.xml_parser

import android.os.Parcelable
import ch.heig.soo_gui.xml_parser.element.Layout
import kotlinx.parcelize.Parcelize

@Parcelize
data class Model(
    val spid: String? = null,
    val name: String? = null,
    val description: String? = null,
    val layout: Layout
) : Parcelable
