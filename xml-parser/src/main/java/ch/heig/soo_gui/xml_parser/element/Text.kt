package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder

@Parcelize
data class Text(override val id: String? = null, var content: String = "") : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "text")
            val id = parser.getAttributeValue(ns, "id")
            val content = XmlHelper.readText(parser)
            parser.require(XmlPullParser.END_TAG, ns, "text")
            return Text(id, content)
        }
    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        val newText = when(type) {
            Message.Type.REPLACE -> StringBuilder()
            Message.Type.PUSH -> StringBuilder(content)
        }

        // Concat all the new texts.
        for (element in elements){
            if (element !is Text) {
                throw IllegalArgumentException("Element in Text cannot be different of Text.")
            }

            newText.append(element.content)
        }
        content = newText.toString()
    }
}
