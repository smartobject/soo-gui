package ch.heig.soo_gui.xml_parser

import ch.heig.soo_gui.xml_parser.element.Element

data class Message(val to: String, val content: List<Element> = emptyList(), val type: Type = Type.REPLACE) {
    enum class Type {
        REPLACE,
        PUSH;

        companion object {
            fun from(value: String?): Type {
                return when (value?.lowercase()) {
                    "push" -> PUSH
                    else -> REPLACE
                }
            }
        }
    }
}
