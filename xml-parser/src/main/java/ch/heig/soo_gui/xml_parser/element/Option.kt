package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.Message
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder

@Parcelize
data class Option(var content: String, val value: String, val default: Boolean = false) : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "option")

            val defaultString = parser.getAttributeValue(ns, "default")
            val default = defaultString != null && (defaultString == "" || defaultString == "true")
            var value = parser.getAttributeValue(ns, "value")
            var content: String? = null

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType == XmlPullParser.TEXT) {
                    // replace if the value isn't declared
                    content = parser.text
                    if (value == null) {
                        value = parser.text
                    }
                }
            }

            if (content.isNullOrBlank()) {
                content = value
            }

            parser.require(XmlPullParser.END_TAG, ns, "option")

            return Option(content ?: "", value ?: "", default)
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        val newText = when(type) {
            Message.Type.REPLACE -> StringBuilder()
            Message.Type.PUSH -> StringBuilder(content)
        }

        // Concat all the new texts.
        for (element in elements){
            if (element !is Text) {
                throw IllegalArgumentException("Element in Option cannot be different of Text.")
            }

            newText.append(element.content)
        }
        content = newText.toString()
    }
}
