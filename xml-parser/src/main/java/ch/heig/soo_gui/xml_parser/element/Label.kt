package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder

@Parcelize
data class Label(val forId: String, var content: String = "") : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "label")
            val forId = parser.getAttributeValue(ns, "for")

            if (forId.isNullOrBlank()) {
                while (parser.next() != XmlPullParser.END_TAG) {}
                if (parser.name == "label") {
                    parser.next()
                }
                throw IllegalArgumentException("The attribute 'for' in <label> cannot be null or blank.")
            }

            val content = XmlHelper.readText(parser)

            parser.require(XmlPullParser.END_TAG, ns, "label")
            return Label(forId, content)
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        val newText = when(type) {
            Message.Type.REPLACE -> StringBuilder()
            Message.Type.PUSH -> StringBuilder(content)
        }

        // Concat all the new texts.
        for (element in elements){
            if (element !is Text) {
                throw IllegalArgumentException("Element in Label cannot be different of Text.")
            }

            newText.append(element.content)
        }
        content = newText.toString()
    }
}
