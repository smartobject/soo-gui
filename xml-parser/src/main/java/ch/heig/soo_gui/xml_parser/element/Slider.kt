package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.Message
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder

@Parcelize
data class Slider(
    override val id: String,
    val min: Double,
    val max: Double,
    val step: Double,
    val orientation: Orientation,
    var value: Double
) :
    Element {
    enum class Orientation {
        HORIZONTAL,
        VERTICAL;

        companion object {
            fun from(value: String?): Orientation =
                if (value.isNullOrBlank() || value.lowercase() != "vertical") {
                    HORIZONTAL
                } else {
                    VERTICAL
                }
        }
    }

    companion object : Element.ElementCompanion {

        @Suppress("NAME_SHADOWING")
        operator fun invoke(
            id: String?,
            min: Double?,
            max: Double?,
            step: Double?,
            orientation: String?,
            value: Double?
        ): Slider {
            if (id.isNullOrBlank()) {
                throw IllegalArgumentException("The value 'id' cannot be null or blank.")
            }

            val min = min ?: 0.0
            val max = max ?: 1.0
            if (min >= max) {
                throw java.lang.IllegalArgumentException("The value of 'min' ($min) cannot be higher or equal to 'max' ($max).")
            }

            val step = if (step == null || step <= 0.0) 0.1 else step


            return Slider(id, min, max, step, Orientation.from(orientation), value ?: 0.0)
        }

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "slider")
            val id = parser.getAttributeValue(ns, "id")
            val min = parser.getAttributeValue(ns, "min")?.toDoubleOrNull()
            val max = parser.getAttributeValue(ns, "max")?.toDoubleOrNull()
            val step = parser.getAttributeValue(ns, "step")?.toDoubleOrNull()
            var value = parser.getAttributeValue(ns, "value")?.toDoubleOrNull()
            val orientation = parser.getAttributeValue(ns, "orientation")


            if (id.isNullOrBlank()) {
                while (parser.next() != XmlPullParser.END_TAG) {}
                if (parser.name == "slider") {
                    parser.next()
                }
                throw IllegalArgumentException("The attribute 'id' in <slider> cannot be null or blank.")
            }

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType == XmlPullParser.TEXT) {
                    // replace if the value isn't declared
                    if (value == null) {
                        value = parser.text.toDoubleOrNull()
                    }
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "slider")
            return Slider(id, min, max, step, orientation, value ?: 0.0)
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        val newText = when(type) {
            Message.Type.REPLACE -> StringBuilder()
            Message.Type.PUSH -> StringBuilder(value.toString())
        }

        // Concat all the new texts.
        for (element in elements){
            if (element !is Text) {
                throw IllegalArgumentException("Element in Slider cannot be different of Text.")
            }

            newText.append(element.content)
        }
        value = newText.toString().toDouble()
    }
}
