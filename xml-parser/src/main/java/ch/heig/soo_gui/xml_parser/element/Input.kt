package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.Message
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder

@Parcelize
data class Input(override val id: String, var value: String) : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "input")
            val id = parser.getAttributeValue(ns, "id")
            var value = parser.getAttributeValue(ns, "value")


            if (id.isNullOrBlank()) {
                while (parser.next() != XmlPullParser.END_TAG) {}
                if (parser.name == "input") {
                    parser.next()
                }
                throw IllegalArgumentException("The attribute 'id' in <input> cannot be null or blank.")
            }

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType == XmlPullParser.TEXT) {
                    // replace if the value isn't declared
                    if (value.isNullOrBlank()) {
                        value = parser.text
                    }
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "input")
            return Input(id, value ?: "")
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        val newText = when(type) {
            Message.Type.REPLACE -> StringBuilder()
            Message.Type.PUSH -> StringBuilder(value)
        }

        // Concat all the new texts.
        for (element in elements){
            if (element !is Text) {
                throw IllegalArgumentException("Element in Input cannot be different of Text.")
            }

            newText.append(element.content)
        }
        value = newText.toString()
    }
}
