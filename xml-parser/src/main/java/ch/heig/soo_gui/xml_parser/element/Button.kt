package ch.heig.soo_gui.xml_parser.element

import android.os.Parcelable
import ch.heig.soo_gui.xml_parser.Message
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder

@Parcelize
data class Button(
    override val id: String,
    var text: String,
    val lockable: Boolean,
    val lockableAfter: LockableAfter
) :
    Element {
    sealed class LockableAfter : Parcelable {
        @Parcelize
        @Suppress("ClassName")
        object ON_CLICK : LockableAfter()

        @Parcelize
        @Suppress("ClassName")
        object ON_DOUBLE_CLICK : LockableAfter()

        @Parcelize
        data class SECONDS(val duration: Double) : LockableAfter()

        companion object {
            fun from(value: String?): LockableAfter {
                return when (value) {
                    "onClick" -> ON_CLICK
                    "onDoubleClick" -> ON_DOUBLE_CLICK
                    else ->
                        if (
                            value.isNullOrBlank() ||
                            value.toDoubleOrNull() == null ||
                            value.toDouble() <= 0.0
                        ) SECONDS(3.0)
                        else SECONDS(value.toDouble())
                }
            }
        }
    }

    companion object : Element.ElementCompanion {

        @Suppress("NAME_SHADOWING")
        operator fun invoke(
            id: String? = null,
            text: String? = null,
            lockable: String? = null,
            lockableAfter: String? = null
        ): Button {
            if (id.isNullOrBlank()) {
                throw IllegalArgumentException("The value 'id' cannot be null or blank.")
            }

            val lockable = lockable != null && (lockable == "" || lockable == "true")
            val lockableAfter = LockableAfter.from(lockableAfter)

            return Button(id, text ?: "", lockable, lockableAfter)
        }

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "button")

            val id = parser.getAttributeValue(ns, "id")
            if (id.isNullOrBlank()) {
                while (parser.next() != XmlPullParser.END_TAG) {}
                if (parser.name == "button") {
                    parser.next()
                }
                throw IllegalArgumentException("The attribute 'id' in <button> cannot be null or blank.")
            }

            val lockable = parser.getAttributeValue(ns, "lockable")
            val lockableAfter = parser.getAttributeValue(ns, "lockable-after")

            var text = ""
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType == XmlPullParser.TEXT) {
                    // replace if the value isn't declared
                    text = parser.text
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "button")

            return Button(id, text, lockable, lockableAfter)
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        val newText = when(type) {
            Message.Type.REPLACE -> StringBuilder()
            Message.Type.PUSH -> StringBuilder(text)
        }

        // Concat all the new texts.
        for (element in elements){
            if (element !is Text) {
                throw IllegalArgumentException("Element in Button cannot be different of Text.")
            }

            newText.append(element.content)
        }
        text = newText.toString()
    }
}
