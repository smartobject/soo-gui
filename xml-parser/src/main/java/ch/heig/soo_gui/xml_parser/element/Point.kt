package ch.heig.soo_gui.xml_parser.element

import android.util.Log
import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Point(var items: List<Item>) : Element {
    companion object : Element.ElementCompanion {
        private val TAG: String = "Point"

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "point")

            val items = listOf<Item>().toMutableList()

            while (parser.nextTag() != XmlPullParser.END_TAG) {
                // if the current element isn't a tag, then skip
                if (parser.eventType == XmlPullParser.END_DOCUMENT) {
                    throw Exception("End of document reached")
                } else if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                when (parser.name) {
                    "item" -> {
                        // partial addition. If error then it doesn't add it
                        try {
                            val item: Item = Item.readElement(parser, ns) as Item
                            items.add(item)
                        } catch (e: Exception) {
                            Log.e(TAG, "cannot parse Value in Item", e)
                        }
                    }
                    else -> XmlHelper.skip(parser)
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "point")

            return Point(items)
        }
    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (item in items) {
            list += item.toList()
        }
        return list
    }

    @Suppress("UNCHECKED_CAST")
    override fun update(elements: List<Element>, type: Message.Type) {
        if (elements.any { e -> e !is Item }) {
            throw IllegalArgumentException("Element in Point cannot be different of Item.")
        }

        when (type) {
            Message.Type.REPLACE -> items = elements as List<Item>
            Message.Type.PUSH -> items += elements as List<Item>
        }
    }
}
