package ch.heig.soo_gui.xml_parser.element

import android.os.Parcelable
import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder

@Parcelize
data class Axis(var text: String, val type: Type) : Element {

    sealed class Type : Parcelable {
        @Parcelize
        @Suppress("ClassName")
        object NUMBER : Type()

        @Parcelize
        @Suppress("ClassName")
        object STRING : Type()

        @Parcelize
        data class DATETIME(val format: String?) : Type()

        companion object {
            fun from(value: String?, format: String?): Type {
                return when (value) {
                    "datetime" -> DATETIME(format)
                    "string" -> STRING
                    else -> NUMBER
                }
            }
        }
    }

    companion object : Element.ElementCompanion {

        @Suppress("NAME_SHADOWING")
        operator fun invoke(
            text: String? = null,
            type: String? = null,
            format: String? = null,
        ): Axis {

            val text = text ?: ""
            val type = Type.from(type, format)

            return Axis(text, type)
        }

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "axis")


            val type = parser.getAttributeValue(ns, "type")
            val format = parser.getAttributeValue(ns, "format")

            val text = XmlHelper.readText(parser)

            parser.require(XmlPullParser.END_TAG, ns, "axis")

            return Axis(text, type, format)
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        val newText = when(type) {
            Message.Type.REPLACE -> StringBuilder()
            Message.Type.PUSH -> StringBuilder(text)
        }

        // Concat all the new texts.
        for (element in elements){
            if (element !is Text) {
                throw IllegalArgumentException("Element in Axis cannot be different of Text.")
            }

            newText.append(element.content)
        }
        text = newText.toString()
    }
}
