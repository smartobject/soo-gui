package ch.heig.soo_gui.xml_parser

import java.lang.StringBuilder

class XmlWriter {
    companion object {
        fun writeEvents(events: List<Event>): String {
            val xml = StringBuilder()
            xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
            xml.append("<events>")
            for (event in events) {
                xml.append(event.toXMLString())
            }
            xml.append("</events>")

            return xml.toString()
        }
    }
}