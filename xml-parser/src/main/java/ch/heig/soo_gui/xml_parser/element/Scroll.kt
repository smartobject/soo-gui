package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder


@Parcelize
data class Scroll(override val id: String?, var history: MutableList<ChatMessage>) : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "scroll")
            val id = parser.getAttributeValue(ns, "id")

            while (parser.next() != XmlPullParser.END_TAG) {
            }
            parser.require(XmlPullParser.END_TAG, ns, "scroll")

            val history = listOf<ChatMessage>().toMutableList()

            return Scroll(id, history)
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    fun clearHistory() {
        history.clear()
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        // Concat all the new texts.
        for (element in elements){
            if (element !is ChatMessage) {
                throw IllegalArgumentException("Element in Label cannot be different of Text.")
            }
            this.history += element
        }
    }
}
