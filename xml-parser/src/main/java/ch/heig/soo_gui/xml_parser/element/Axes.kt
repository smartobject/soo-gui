package ch.heig.soo_gui.xml_parser.element

import android.util.Log
import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder

@Parcelize
data class Axes(val displaySeriesName: Boolean = false, var axes: List<Axis>) : Element {

    companion object : Element.ElementCompanion {
        private val TAG: String = "Row"

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "axes")

            val displaySeriesNameAttribute = parser.getAttributeValue(ns, "display-series-name")
            val displaySeriesName = displaySeriesNameAttribute != null && displaySeriesNameAttribute == "true"
            val axes = listOf<Axis>().toMutableList()

            while (parser.nextTag() != XmlPullParser.END_TAG) {
                // if the current element isn't a tag, then skip
                if (parser.eventType == XmlPullParser.END_DOCUMENT) {
                    throw Exception("End of document reached")
                } else if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                when (parser.name) {
                    "axis" -> {
                        // partial addition. If error then it doesn't add it
                        try {
                            val axis: Axis = Axis.readElement(parser, ns) as Axis
                            axes.add(axis)
                        } catch (e: Exception) {
                            Log.e(TAG, "cannot parse Axis in Axes", e)
                        }
                    }
                    else -> XmlHelper.skip(parser)
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "axes")
            return Axes(displaySeriesName, axes)
        }
    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (axis in axes) {
            list += axis.toList()
        }
        return list
    }

    @Suppress("UNCHECKED_CAST")
    override fun update(elements: List<Element>, type: Message.Type) {
        if (elements.any { e -> e !is Axis}) {
            throw IllegalArgumentException("Element in Axes cannot be different of Axis.")
        }

        when (type) {
            Message.Type.REPLACE -> axes = elements as List<Axis>
            Message.Type.PUSH -> axes += elements as List<Axis>
        }
    }
}
