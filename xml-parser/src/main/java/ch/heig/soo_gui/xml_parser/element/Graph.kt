package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Graph(
    override val id: String,
    val type: Type,
    var axes: Axes,
    var series: List<Series> = emptyList()
) : Element {
    enum class Type {
        LINE,
        TABLE;

        companion object {
            fun from(value: String?): Type {
                return when (value) {
                    "line" -> LINE
                    else -> TABLE
                }
            }
        }
    }

    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "graph")

            val id = parser.getAttributeValue(ns, "id")
            if (id.isNullOrBlank()) {
                while (parser.next() != XmlPullParser.END_TAG) {
                    if(parser.eventType == XmlPullParser.END_DOCUMENT) {
                        break
                    }
                }
                if (parser.name == "graph") {
                    parser.next()
                }
                throw IllegalArgumentException("The attribute 'id' in <graph> cannot be null or blank.")
            }

            val typeString = parser.getAttributeValue(ns, "type")
            val type = Type.from(typeString)

            var axes: Axes? = null
            val series = listOf<Series>().toMutableList()

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType == XmlPullParser.END_DOCUMENT) {
                    throw Exception("End of document reached")
                } else if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                when (parser.name) {
                    "series" -> series.add(Series.readElement(parser, ns) as Series)
                    "axes" -> axes = Axes.readElement(parser, ns) as Axes
                    else -> XmlHelper.skip(parser)
                }

            }

            parser.require(XmlPullParser.END_TAG, ns, "graph")

            if (axes == null) {
                throw IllegalArgumentException("The field <axes> in <graph> is missing.")
            }

            return Graph(id, type, axes, series)
        }

    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (serie in series) {
            list += serie.toList()
        }
        return list
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        if (elements.any { e -> e !is Axes && e !is Series }) {
            throw IllegalArgumentException("Element in Graph cannot be different of Axes or Series.")
        }

        when (type) {
            Message.Type.REPLACE -> {
                this.axes = elements.find { e -> e is Axes } as Axes
                this.series = elements.filterIsInstance<Series>()
            }
            Message.Type.PUSH -> {
                this.axes.update((elements.find { e -> e is Axes } as Axes).axes,
                    Message.Type.PUSH)
                this.series += elements.filterIsInstance<Series>()
            }
        }
    }
}
