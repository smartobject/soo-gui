package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.Message
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder

@Parcelize
data class Number(override val id: String, val step: Double, var value: Double) : Element {
    companion object : Element.ElementCompanion {

        @Suppress("NAME_SHADOWING")
        operator fun invoke(
            id: String? = null,
            step: Double? = null,
            value: Double? = null,
        ): Number {
            if (id.isNullOrBlank()) {
                throw IllegalArgumentException("The value 'id' cannot be null or blank.")
            }
            val step = if (step == null || step <= 0.0) 1.0 else step
            return Number(id, step, value ?: 0.0)
        }

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "number")
            val id = parser.getAttributeValue(ns, "id")
            val step = parser.getAttributeValue(ns, "step")?.toDoubleOrNull()
            var value = parser.getAttributeValue(ns, "value")?.toDoubleOrNull()


            if (id.isNullOrBlank()) {
                while (parser.next() != XmlPullParser.END_TAG) {}
                if (parser.name == "number") {
                    parser.next()
                }
                throw IllegalArgumentException("The attribute 'id' in <number> cannot be null or blank.")
            }

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType == XmlPullParser.TEXT) {
                    // replace if the value isn't declared
                    if (value == null) {
                        value = parser.text.toDoubleOrNull()
                    }
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "number")
            return Number(id, step, value ?: 0.0)
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        val newText = when(type) {
            Message.Type.REPLACE -> StringBuilder()
            Message.Type.PUSH -> StringBuilder(value.toString())
        }

        // Concat all the new texts.
        for (element in elements){
            if (element !is Text) {
                throw IllegalArgumentException("Element in Number cannot be different of Text.")
            }

            newText.append(element.content)
        }
        value = newText.toString().toDouble()
    }
}
