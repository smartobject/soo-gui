package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser
import java.lang.StringBuilder

@Parcelize
data class ChatMessage(var sender: Int, var text: String) : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "chat")
            val from = parser.getAttributeValue(ns, "from").toInt()

            val content = XmlHelper.readText(parser)

            parser.require(XmlPullParser.END_TAG, ns, "chat")
            return ChatMessage(from, content)
        }
    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>, type: Message.Type) {
        for (element in elements){
            if (element !is Text) {
                throw IllegalArgumentException("Element in Label cannot be different of Text.")
            }
        }
    }
}
