package ch.heig.soo_gui.xml_parser.element

import android.util.Log
import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Layout(override val id: String?, val maxCols: Int, var rows: List<Row>) : Element {
    companion object : Element.ElementCompanion {
        private val TAG: String = "Layout"

        @Suppress("NAME_SHADOWING")
        operator fun invoke(
            id: String? = null,
            maxCols: Int? = null,
            rows: List<Row>? = null
        ): Layout {
            val maxCols = if (maxCols == null || maxCols < 1) 12 else maxCols
            return Layout(id, maxCols, rows ?: emptyList())
        }

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "layout")
            val id = parser.getAttributeValue(ns, "id")
            val maxCols = parser.getAttributeValue(ns, "cols")?.toIntOrNull()

            val rows = listOf<Row>().toMutableList()

            while (parser.nextTag() != XmlPullParser.END_TAG) {
                // if the current element isn't a tag, then skip
                if (parser.eventType == XmlPullParser.END_DOCUMENT) {
                    throw Exception("End of document reached")
                } else if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                when (parser.name) {
                    "row" -> {
                        // partial addition. If error then it doesn't add it
                        try {
                            val row: Row = Row.readElement(parser, ns) as Row
                            rows.add(row)
                        } catch (e: Exception) {
                            Log.e(TAG, "cannot parse Row in Layout", e)
                        }
                    }
                    else -> XmlHelper.skip(parser)
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "layout")
            return Layout(id, maxCols, rows)
        }
    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (row in rows) {
            list += row.toList()
        }
        return list
    }

    @Suppress("UNCHECKED_CAST")
    override fun update(elements: List<Element>, type: Message.Type) {
        if (elements.any { e -> e !is Row }) {
            throw IllegalArgumentException("Element in Layout cannot be different of Row.")
        }

        when (type) {
            Message.Type.REPLACE -> rows = elements as List<Row>
            Message.Type.PUSH -> rows += elements as List<Row>
        }
    }
}
