package ch.heig.soo_gui.xml_parser

import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import java.io.IOException

object XmlHelper {
    /**
     * Create an XML parser.
     *
     * @param xml XML to analyse.
     * @param ns Namespace.
     * @return XML parser.
     */
    fun buildParser(xml: String, ns: String? = null): XmlPullParser {
        val inputStream = xml.byteInputStream()
        val parserFactory = XmlPullParserFactory.newInstance()
        val parser: XmlPullParser = parserFactory.newPullParser()
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, ns != null)
        parser.setInput(inputStream, null)
        parser.nextTag()
        return parser

    }

    /**
     * Read the text in between 2 tags.
     *
     * @param parser Parser containing the text.
     * @return The text as a string.
     */
    fun readText(parser: XmlPullParser): String {
        var text = ""
        if (parser.next() == XmlPullParser.TEXT) {
            text = parser.text
            parser.nextTag()
        }
        return text
    }

    /**
     * Skip the next tags and all its children.
     *
     * @param parser Parser needed to be skipped.
     */
    @Throws(XmlPullParserException::class, IOException::class)
    fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }
}