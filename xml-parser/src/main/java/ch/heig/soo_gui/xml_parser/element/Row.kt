package ch.heig.soo_gui.xml_parser.element

import android.util.Log
import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Row(override val id: String?, var cols: List<Col> = emptyList()) : Element {
    companion object : Element.ElementCompanion {
        private val TAG: String = "Row"

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "row")
            val id = parser.getAttributeValue(ns, "id")

            val cols = listOf<Col>().toMutableList()

            while (parser.nextTag() != XmlPullParser.END_TAG) {
                // if the current element isn't a tag, then skip
                if (parser.eventType == XmlPullParser.END_DOCUMENT) {
                    throw Exception("End of document reached")
                } else if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                when (parser.name) {
                    "col" -> {
                        // partial addition. If error then it doesn't add it
                        try {
                            val col: Col = Col.readElement(parser, ns) as Col
                            cols.add(col)
                        } catch (e: Exception) {
                            Log.e(TAG, "cannot parse Col in Row", e)
                        }
                    }
                    else -> XmlHelper.skip(parser)
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "row")
            return Row(id, cols)
        }
    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (col in cols) {
            list += col.toList()
        }
        return list
    }

    @Suppress("UNCHECKED_CAST")
    override fun update(elements: List<Element>, type: Message.Type) {
        if (elements.any { e -> e !is Col }) {
            throw IllegalArgumentException("Element in Row cannot be different of Col.")
        }

        when (type) {
            Message.Type.REPLACE -> cols = elements as List<Col>
            Message.Type.PUSH -> cols += elements as List<Col>
        }
    }
}
