package ch.heig.soo_gui.xml_parser.element

import android.os.Build
import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class ItemTest {
    val ns: String? = null

    @Test
    fun completeItemXMLShouldBeParsed() {
        val xml = "<item>Test</item>"

        val parser = XmlHelper.buildParser(xml, ns)

        val itemParsed = Item.readElement(parser, ns)

        Assert.assertEquals(
            Item(value="Test"),
            itemParsed
        )
    }

    @Test
    fun emptyItemXMLShouldBeParsed() {
        val xml = "<item></item>"

        val parser = XmlHelper.buildParser(xml, ns)

        val itemParsed = Item.readElement(parser, ns)

        Assert.assertEquals(
            Item(value=""),
            itemParsed
        )
    }
}