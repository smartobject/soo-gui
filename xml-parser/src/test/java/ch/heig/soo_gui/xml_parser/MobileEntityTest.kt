package ch.heig.soo_gui.xml_parser

import android.os.Build
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

/**
 * Unit test of the mobile entity received.
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class MobileEntityTest {
    @Test
    fun completeMobileEntityShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "<mobile-entity spid=\"10f1e79f-486b-4103-bd01-f9b8864479cf\">" +
                "<name>Test</name>" +
                "<description>Short description</description>" +
                "</mobile-entity>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        assertEquals(XmlParser.ContentType.MOBILE_ENTITIES, result.type)
        assertEquals(
            listOf(
                MobileEntity(
                    spid = "10f1e79f-486b-4103-bd01-f9b8864479cf",
                    name = "Test",
                    description = "Short description"
                )
            ), result.content
        )
    }

    @Test
    fun emptyMobileEntitiesShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        assertEquals(XmlParser.ContentType.MOBILE_ENTITIES, result.type)
        assertEquals(emptyList<MobileEntity>(), result.content)
    }

    @Test
    fun manyMobileEntitiesShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "<mobile-entity spid=\"4278c640-3fdc-40af-8d74-7b9a0514b556\">" +
                "<name>Test 1</name>" +
                "<description>Short description 1</description>" +
                "</mobile-entity>" +
                "<mobile-entity spid=\"212b7b9e-e7f1-4291-b557-649a3dd6efa4\">" +
                "<name>Test 2</name>" +
                "<description>Short description 2</description>" +
                "</mobile-entity>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        assertEquals(XmlParser.ContentType.MOBILE_ENTITIES, result.type)
        assertEquals(
            listOf(
                MobileEntity(
                    spid = "4278c640-3fdc-40af-8d74-7b9a0514b556",
                    name = "Test 1",
                    description = "Short description 1"
                ),
                MobileEntity(
                    spid = "212b7b9e-e7f1-4291-b557-649a3dd6efa4",
                    name = "Test 2",
                    description = "Short description 2"
                ),
            ), result.content
        )
    }

    @Test
    fun mobileEntityWithoutSpidAttributeShouldNotBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "<mobile-entity spid=\"48c122f1-0103-4653-b71c-68ee2be8d00f\">" +
                "<name>Test 1</name>" +
                "<description>Short description 1</description>" +
                "</mobile-entity>" +
                "<mobile-entity>" +
                "<name>Test 2</name>" +
                "<description>Short description 2</description>" +
                "</mobile-entity>" +
                "<mobile-entity spid=\"d1f93ba2-6648-4e0d-9d36-f392f8858c31\">" +
                "<name>Test 3</name>" +
                "<description>Short description 3</description>" +
                "</mobile-entity>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        assertEquals(
            listOf(
                MobileEntity(
                    spid = "48c122f1-0103-4653-b71c-68ee2be8d00f",
                    name = "Test 1",
                    description = "Short description 1"
                ),
                MobileEntity(
                    spid = "d1f93ba2-6648-4e0d-9d36-f392f8858c31",
                    name = "Test 3",
                    description = "Short description 3"
                ),
            ), result.content
        )
    }

    @Test
    fun mobileEntityWithoutNameShouldNotBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "<mobile-entity spid=\"c9f55091-13b9-4cb0-8d80-a730ace5d0db\">" +
                "<name>Test 1</name>" +
                "<description>Short description 1</description>" +
                "</mobile-entity>" +
                "<mobile-entity spid=\"078c15b1-f3b8-4d59-bbe4-4e75dd2aef4c\">" +
                "<description>Short description 2</description>" +
                "</mobile-entity>" +
                "<mobile-entity spid=\"2d0e4620-6c3b-472e-af03-073985b381b4\">" +
                "<name>Test 3</name>" +
                "<description>Short description 3</description>" +
                "</mobile-entity>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        assertEquals(
            listOf(
                MobileEntity(
                    spid = "c9f55091-13b9-4cb0-8d80-a730ace5d0db",
                    name = "Test 1",
                    description = "Short description 1"
                ),
                MobileEntity(
                    spid = "2d0e4620-6c3b-472e-af03-073985b381b4",
                    name = "Test 3",
                    description = "Short description 3"
                ),
            ), result.content
        )
    }

    @Test
    fun mobileEntityWithoutDescriptionShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "<mobile-entity spid=\"26bfd4be-c0e8-4355-974e-3abe3cdd2057\">" +
                "<name>Test</name>" +
                "</mobile-entity>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        assertEquals(XmlParser.ContentType.MOBILE_ENTITIES, result.type)
        assertEquals(
            listOf(
                MobileEntity(
                    spid = "26bfd4be-c0e8-4355-974e-3abe3cdd2057",
                    name = "Test",
                    description = null
                )
            ), result.content
        )
    }
}


