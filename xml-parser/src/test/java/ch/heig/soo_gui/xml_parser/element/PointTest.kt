package ch.heig.soo_gui.xml_parser.element

import android.os.Build
import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class PointTest {
    val ns: String? = null

    @Test
    fun completePointShouldBeParsed() {
        val xml = "<point>" +
                "<item>xValue</item>" +
                "<item>yValue</item>" +
                "<item>zValue</item>" +
                "</point>"

        val parser = XmlHelper.buildParser(xml, ns)

        val pointParsed = Point.readElement(parser, ns)

        Assert.assertEquals(
            Point(
                listOf(
                    Item("xValue"),
                    Item("yValue"),
                    Item("zValue"),
                )
            ),
            pointParsed
        )
    }

    @Test
    fun emptyPointShouldBeParsed() {
        val xml = "<point/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val pointParsed = Point.readElement(parser, ns)

        Assert.assertEquals(
            Point(listOf()),
            pointParsed
        )
    }
}