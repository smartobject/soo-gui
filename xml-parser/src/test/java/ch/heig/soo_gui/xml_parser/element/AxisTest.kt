package ch.heig.soo_gui.xml_parser.element

import android.os.Build
import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class AxisTest {
    val ns: String? = null

    @Test
    fun completeAxisXMLShouldBeParsed() {
        val xml = "<axis type=\"number\">Test</axis>"

        val parser = XmlHelper.buildParser(xml, ns)

        val axisParsed = Axis.readElement(parser, ns)

        Assert.assertEquals(
            Axis(
                type = Axis.Type.NUMBER,
                text = "Test"
            ),
            axisParsed
        )
    }

    @Test
    fun emptyAxisXMLShouldBeParsed() {
        val xml = "<axis/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val axisParsed = Axis.readElement(parser, ns)

        Assert.assertEquals(
            Axis(
                type = Axis.Type.NUMBER,
                text = ""
            ),
            axisParsed
        )
    }

    @Test
    fun everyPossibleTypeValueShouldBeParsed() {
        val xmls = arrayOf(
            "<axis type=\"number\">Test 1</axis>",
            "<axis type=\"string\">Test 2</axis>",
            "<axis type=\"datetime\">Test 3</axis>",
            "<axis type=\"datetime\" format=\"hh:mm\">Test 4</axis>",
        )

        val expected = arrayOf(
            Axis(
                type = Axis.Type.NUMBER,
                text = "Test 1",
            ),
            Axis(
                type = Axis.Type.STRING,
                text = "Test 2",
            ),
            Axis(
                type = Axis.Type.DATETIME(null),
                text = "Test 3",
            ),
            Axis(
                type = Axis.Type.DATETIME("hh:mm"),
                text = "Test 4",
            ),
        )

        for (i in xmls.indices) {
            val parser = XmlHelper.buildParser(xmls[i], ns)

            val axisParsed = Axis.readElement(parser, ns)

            Assert.assertEquals(expected[i], axisParsed)
        }
    }
}