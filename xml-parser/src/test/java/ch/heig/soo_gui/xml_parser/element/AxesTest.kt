package ch.heig.soo_gui.xml_parser.element

import android.os.Build
import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class AxesTest {
    val ns: String? = null

    @Test
    fun completeAxesXMLShouldBeParsed() {
        val xml = "<axes display-series-name=\"false\">" +
                "<axis/>" +
                "</axes>"

        val parser = XmlHelper.buildParser(xml, ns)

        val axesParsed = Axes.readElement(parser, ns)

        Assert.assertEquals(
            Axes(
                displaySeriesName = false,
                listOf(
                    Axis()
                )
            ),
            axesParsed
        )
    }

    @Test
    fun emptyAxesXMLShouldBeParsed() {
        val xml = "<axes/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val axesParsed = Axes.readElement(parser, ns)

        Assert.assertEquals(
            Axes(
                displaySeriesName = false,
                listOf()
            ),
            axesParsed
        )
    }
}