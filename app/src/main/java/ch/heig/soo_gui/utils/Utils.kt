package ch.heig.soo_gui.utils

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.res.ColorStateList
import android.util.TypedValue
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ch.heig.soo_gui.MainActivity
import ch.heig.soo_gui.pages.MobileEntityPageFragment
import ch.heig.soo_gui.utils.Utils.getActivity
import ch.heig.soo_gui.utils.Utils.getCurrentNavigationFragment
import com.heig.soo_gui.R
import java.text.Normalizer

object Utils {
    tailrec fun Context.getActivity(): Activity? = this as? Activity
        ?: (this as? ContextWrapper)?.baseContext?.getActivity()

    val FragmentManager.getCurrentNavigationFragment: Fragment?
        get() = primaryNavigationFragment?.childFragmentManager?.fragments?.first()

    fun Context.getColorFromAttr(attr: Int): Int {
        val textColorTypedValue = TypedValue()
        this.theme.resolveAttribute(attr, textColorTypedValue, true)
        return textColorTypedValue.data
    }

    fun Context.getCurrentNavigationFragment(): Fragment? {
        val activity = (this.getActivity() as MainActivity?)
        return activity?.supportFragmentManager
            ?.getCurrentNavigationFragment
    }

    fun Context.getColors(resId: Int): ColorStateList {
        return AppCompatResources.getColorStateList(this, resId)
    }

    fun Context.dpToPixel(value: Float): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, resources.displayMetrics)
    }

    /**
     * Remove the accents.
     * Source : https://stackoverflow.com/a/51734018
     *
     * @return Unaccented text.
     */
    fun CharSequence.unaccented(): String {
        val REGEX_UNACCENT = "\\p{InCombiningDiacriticalMarks}+".toRegex()
        val temp = Normalizer.normalize(this, Normalizer.Form.NFD)
        return REGEX_UNACCENT.replace(temp, "")
    }
}