package ch.heig.soo_gui.widgets

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.ScrollView

/**
 * A view group that allows the view hierarchy placed within it to be scrolled.
 * The scroll can be locked or unlocked at will.
 * SOURCE [Quabr](http://5.9.10.113/65543781/how-to-disable-work-of-scrollview-on-inner-seekbar)
 */
class LockableScrollView : ScrollView {

    // true if we can scroll (not locked)
    // false if we cannot scroll (locked)
    private var mScrollable = true

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context)

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    )

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    /**
     * Enable or disable the scrolling of the view.
     *
     * @param enabled True if the view can be scrolled, false otherwise.
     */
    fun setScrollingEnabled(enabled: Boolean) {
        mScrollable = enabled
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return if (ev.action == MotionEvent.ACTION_DOWN) { // if we can scroll pass the event to the superclass
            mScrollable && super.onTouchEvent(ev)
        } else super.onTouchEvent(ev)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        // Don't do anything with intercepted touch events if
        // we are not scrollable
        return mScrollable && super.onInterceptTouchEvent(ev)
    }
}