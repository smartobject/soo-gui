package ch.heig.soo_gui.widgets.elements

interface ElementUI {
    fun notifyUpdate(id: String)
}