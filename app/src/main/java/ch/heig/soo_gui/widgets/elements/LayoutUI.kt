package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.TableLayout
import androidx.core.view.children
import androidx.lifecycle.MutableLiveData
import ch.heig.soo_gui.xml_parser.element.Col
import ch.heig.soo_gui.xml_parser.element.Element
import ch.heig.soo_gui.xml_parser.element.Layout
import ch.heig.soo_gui.xml_parser.element.Row

/**
 * Custom layout.
 */
class LayoutUI : TableLayout, ElementUI {

    private var _layout: Layout? = null

    var layout: Layout?
        get() = _layout
        set(value) {
            _layout = value
            constructLayout()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs)
    }

    @Suppress("UNUSED_PARAMETER")
    private fun init(attrs: AttributeSet?) {
        layoutParams = LinearLayout.LayoutParams(
            0, // width
            LinearLayout.LayoutParams.WRAP_CONTENT, // height
            1f
        )
        constructLayout()
    }

    /**
     * Construct the UI from the layout model.
     * Request the new view at the end.
     *
     */
    private fun constructLayout() {
        // clean
        this.removeAllViews()

        if (_layout != null) {
            //redraw
            for (row in _layout!!.rows) {
                val rowView = RowUI(context)
                rowView.row = row
                rowView.maxCols = _layout!!.maxCols
                this.addView(rowView)
            }
        }

        invalidate()
        requestLayout()
    }

    override fun notifyUpdate(id: String) {
        if (_layout?.id == id) {
            constructLayout()
        } else if (_layout != null) {
            for (view in children) {
                if (view is ElementUI) {
                    view.notifyUpdate(id)
                }
            }
        }

        visibility = GONE
        visibility = VISIBLE
    }
}