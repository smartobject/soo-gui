package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.view.View
import ch.heig.soo_gui.xml_parser.element.*
import ch.heig.soo_gui.xml_parser.element.Number

/**
 * Generator of the different available UI elements.
 */
object UIGenerator {
    /**
     * Generate an UI element from the base model and the current context.
     *
     * @param context The context where the fragment is used.
     * @param element The base model of the UI component.
     * @return The corresponding UI element or null if not found.
     */
    fun generateUIElement(context: Context, element: Element): View? {
        return when (element) {
            is Layout -> {
                val layoutView = LayoutUI(context)
                layoutView.layout = element
                layoutView
            }
            is Text -> {
                val textView = TextUI(context)
                textView.textElement = element
                textView
            }
            is Label -> {
                val labelView = LabelUI(context)
                labelView.label = element
                labelView
            }
            is Button -> {
                val buttonView = ButtonUI(context)
                buttonView.button = element
                buttonView
            }
            is Graph -> {
                val graphView = GraphUI(context)
                graphView.graph = element
                graphView
            }
            is Input -> {
                val inputView = InputUI(context)
                inputView.input = element
                inputView
            }
            is Number -> {
                val numberView = NumberUI(context)
                numberView.number = element
                numberView
            }
            is Dropdown -> {
                val dropdownView = DropdownUI(context)
                dropdownView.dropdown = element
                dropdownView
            }
            is Slider -> {
                val sliderView = SliderUI(context)
                sliderView.slider = element
                sliderView
            }
            is Scroll -> {
                val scrollView = ScrollUI(context)
                scrollView.scroll = element
                scrollView
            }
            else -> null
        }
    }
}