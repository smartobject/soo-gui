package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import ch.heig.soo_gui.xml_parser.element.Label
import com.google.android.material.textview.MaterialTextView
import com.heig.soo_gui.R

/**
 * Custom label for UI elements.
 *
 */
class LabelUI : MaterialTextView, ElementUI {

    private var _label: Label? = null

    var label: Label?
        get() = _label
        set(value) {
            _label = value
            constructLabel()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    @Suppress("UNUSED_PARAMETER")
    fun init(attrs: AttributeSet?, defStyle: Int) {
        gravity = Gravity.CENTER_VERTICAL
        layoutParams = LinearLayout.LayoutParams(
            0,
            LinearLayout.LayoutParams.MATCH_PARENT,
            1f
        )
        constructLabel()
    }

    /**
     * Construct the UI from the label model.
     * Request the new view at the end.
     *
     */
    private fun constructLabel() {
//        var text = _label?.content ?: ""
//        text.trimEnd()
//        if (text.isNotBlank()) {
//            if (text.last() != ':') {
//                text += ":"
//            }
//            text += "  "
//        }
//        this.text = text
        text = _label?.content ?: ""

        invalidate()
        requestLayout()
    }

    override fun notifyUpdate(id: String) {
        if (_label?.id == id) {
            constructLabel()
            visibility = GONE
            visibility = VISIBLE
        }
    }
}