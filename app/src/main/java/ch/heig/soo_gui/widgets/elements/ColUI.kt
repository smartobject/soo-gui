package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.core.view.children
import androidx.core.view.setPadding
import ch.heig.soo_gui.utils.Utils.dpToPixel
import ch.heig.soo_gui.xml_parser.element.Col

/**
 * Custom column containing different UI elements.
 *
 */
class ColUI : LinearLayout, ElementUI {

    private var _col: Col? = null

    var col: Col?
        get() = _col
        set(value) {
            _col = value
            constructCol()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    @Suppress("UNUSED_PARAMETER")
    private fun init(attrs: AttributeSet?, defStyle: Int) {
        this.setPadding(context.dpToPixel(2f).toInt())
        constructCol()
    }

    /**
     * Construct the UI from the col model.
     * Request the new view at the end.
     *
     */
    private fun constructCol() {
        // clean
        this.removeAllViews()

        if (_col != null) {
            // do the span attribute
            when (val span = _col!!.span) {
                is Col.Spanning.AUTO -> this.layoutParams = LayoutParams(
                    0, // width
                    LayoutParams.WRAP_CONTENT, // height,
                    1F
                )
                is Col.Spanning.FIXED -> {
                    this.layoutParams = LayoutParams(
                        0, // width
                        LayoutParams.WRAP_CONTENT, // height
                        span.nbCols.toFloat() //weight
                    )
                }
            }


            for (element in _col!!.elements) {
                val elementView = UIGenerator.generateUIElement(context, element)
                if (elementView != null) {
                    this.addView(elementView)
                }
            }
        }// else

        invalidate()
        requestLayout()
    }// constructCol(...)

    override fun notifyUpdate(id: String) {
        if (_col?.id == id) {
            constructCol()
        } else if (_col != null) {
            for (view in this.children) {
                if (view is ElementUI) {
                    view.notifyUpdate(id)
                }
            }
        }

        visibility = GONE
        visibility = VISIBLE
    }
}