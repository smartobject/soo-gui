package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doOnTextChanged
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.pages.MobileEntityInterface
import ch.heig.soo_gui.utils.Utils.getCurrentNavigationFragment
import ch.heig.soo_gui.xml_parser.Event
import ch.heig.soo_gui.xml_parser.element.Input
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.heig.soo_gui.R

/**
 * Custom input.
 *
 */
class InputUI : ConstraintLayout, ElementUI {

    private var _input: Input? = null
    private lateinit var mInputView: TextInputEditText
    private lateinit var mTextInputLayout: TextInputLayout

    var input: Input?
        get() = _input
        set(value) {
            _input = value
            constructInput()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    @Suppress("UNUSED_PARAMETER")
    fun init(attrs: AttributeSet?, defStyle: Int) {
        inflate(context, R.layout.view_input_ui, this)

        // get components
        mInputView = findViewById(R.id.view_input_ui_input)
        mTextInputLayout = findViewById(R.id.view_input_ui_til)

        // set layout
        layoutParams = LinearLayout.LayoutParams(
            0, // width
            LinearLayout.LayoutParams.WRAP_CONTENT, // height
            1f
        )

        val fragment = context.getCurrentNavigationFragment()
        mInputView.doOnTextChanged { text, _, _, _ ->
            _input!!.value = text.toString()
            if (fragment is MobileEntityInterface) {
                fragment.send(
                    MessageType.POST,
                    Event(
                        from = _input!!.id,
                        action = Event.Action.VALUE_CHANGED(text.toString())
                    )
                )
            }
        }


        constructInput()
    }

    /**
     * Construct the UI from the input model.
     * Request the new view at the end.
     *
     */
    private fun constructInput() {
        if (_input == null) {
            mTextInputLayout.visibility = GONE
        } else {
            mTextInputLayout.visibility = VISIBLE
            mInputView.setText(_input!!.value)
        }

        invalidate()
        requestLayout()
    } // constructInput(...)

    override fun notifyUpdate(id: String) {
        if (_input?.id == id) {
            constructInput()
            visibility = GONE
            visibility = VISIBLE
        }
    }
}