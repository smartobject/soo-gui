package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doAfterTextChanged
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.pages.MobileEntityInterface
import ch.heig.soo_gui.utils.Utils.getCurrentNavigationFragment
import ch.heig.soo_gui.xml_parser.Event
import ch.heig.soo_gui.xml_parser.element.Number
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.heig.soo_gui.R

/**
 * Custom number input.
 *
 */
class NumberUI : ConstraintLayout, ElementUI {

    private var _number: Number? = null
    private lateinit var mNumberView: TextInputEditText
    private lateinit var mTextInputLayout: TextInputLayout

    var number: Number?
        get() = _number
        set(value) {
            _number = value
            constructNumber()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    @Suppress("UNUSED_PARAMETER")
    fun init(attrs: AttributeSet?, defStyle: Int) {
        inflate(context, R.layout.view_number_ui, this)

        // get components
        mNumberView = findViewById(R.id.view_number_ui_input)
        mTextInputLayout = findViewById(R.id.view_number_ui_til)

        // set layout
        layoutParams = LinearLayout.LayoutParams(
            0, // width
            LinearLayout.LayoutParams.WRAP_CONTENT, // height
            1f
        )

        constructNumber()
    }

    /**
     * Construct the UI from the number model.
     * Request the new view at the end.
     *
     */
    private fun constructNumber() {
        if (_number == null) {
            mTextInputLayout.visibility = GONE
        } else {
            mTextInputLayout.visibility = VISIBLE
            mNumberView.setText(_number!!.value.toBigDecimal().toPlainString())

            mNumberView.doAfterTextChanged { text ->
                if (text.isNullOrBlank() || !isValidNumber(text.toString())) {

                    // if in regex /^0\d$/ then convert to /^\d$/
                    val numberText = text.toString().toIntOrNull()?.toString()
                    if (numberText != null && isValidNumber(numberText)) {
                        modifyText(numberText)
                    } else {
                        modifyText("0")
                    }

                    return@doAfterTextChanged
                }

                // if new number is different from previous then modify
                if (_number!!.value != text.toString().toDouble()) {
                    _number!!.value = text.toString().toDouble()
                    send(_number!!.value.toString())
                }
            }
        }

        invalidate()
        requestLayout()
    }// constructNumber(...)

    /**
     * Modify the text inside the input.
     *
     * @param numberText New number.
     */
    private fun modifyText(numberText: String) {
        mNumberView.setText(numberText)
        mNumberView.setSelection(numberText.length)

    }

    /**
     * Send the message.
     *
     * @param message New value.
     */
    private fun send(message: String) {
        val fragment = context.getCurrentNavigationFragment()

        if (fragment is MobileEntityInterface) {
            fragment.send(
                MessageType.POST,
                Event(
                    from = _number!!.id,
                    action = Event.Action.VALUE_CHANGED(message)
                )
            )
        }
    }

    /**
     * Check if a text is a valid number.
     *
     * @param text Text to check
     * @return True if it's valid, false otherwise.
     */
    private fun isValidNumber(text: String): Boolean {
        return "^-?((0|[1-9]\\d*)(\\.|\\.\\d+)?|\\.\\d+)$".toRegex()
            .matches(text) && text.toDoubleOrNull() != null
    }

    override fun notifyUpdate(id: String) {
        if (_number?.id == id) {
            constructNumber()
            visibility = GONE
            visibility = VISIBLE
        }
    }
}