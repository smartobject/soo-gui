package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import ch.heig.soo_gui.xml_parser.element.Text
import com.google.android.material.textview.MaterialTextView

/**
 * Custom text displayer.
 *
 */
class TextUI : MaterialTextView, ElementUI {

    private var _text: Text? = null

    var textElement: Text?
        get() = _text
        set(value) {
            _text = value
            constructText()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    @Suppress("UNUSED_PARAMETER")
    fun init(attrs: AttributeSet?, defStyle: Int) {
        layoutParams = LinearLayout.LayoutParams(
            0, // width
            LinearLayout.LayoutParams.WRAP_CONTENT, // height
            1f
        )
        constructText()
    }

    /**
     * Construct the UI from the text model.
     * Request the new view at the end.
     *
     */
    private fun constructText() {
        text = _text?.content ?: ""
        invalidate()
        requestLayout()
    }

    override fun notifyUpdate(id: String) {
        if (_text?.id == id) {
            constructText()
            visibility = GONE
            visibility = VISIBLE
        }
    }
}