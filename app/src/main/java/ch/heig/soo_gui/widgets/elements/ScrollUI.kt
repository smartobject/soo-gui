package ch.heig.soo_gui.widgets.elements


import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.ScrollView
import ch.heig.soo_gui.widgets.ChatCard
import ch.heig.soo_gui.xml_parser.element.Element
import ch.heig.soo_gui.xml_parser.element.Scroll
import com.heig.soo_gui.R

/**
 * Custom label for UI elements.
 *
 */
class ScrollUI : ScrollView, ElementUI {


    private lateinit var mLinearLayout: LinearLayout

    private var _scroll: Scroll? = null
    var scroll: Scroll?
        get() = _scroll
        set(value) {
            _scroll = value
            constructScrollView()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    @Suppress("UNUSED_PARAMETER")
    fun init(attrs: AttributeSet?, defStyle: Int) {
        inflate(context, R.layout.view_scrollview, this)
        constructScrollView()

        mLinearLayout = findViewById(R.id.scrolLinearLayout)
    }


    /**
     * Construct the UI from the label model.
     * Request the new view at the end.
     *
     */
    private fun constructScrollView() {
        if (scroll != null) {
            for (msg in scroll!!.history) {
                val newChat = ChatCard(context)
                newChat.msgTextTv.text = msg.text
                newChat.uidTv.text = msg.sender.toString()
                mLinearLayout.addView(newChat)
            }
            scroll!!.clearHistory()
        }

        invalidate()
        requestLayout()
    }

    override fun notifyUpdate(id: String) {
        if (_scroll?.id == id) {
            constructScrollView()
            visibility = GONE
            visibility = VISIBLE
        }
    }
}