package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Space
import android.widget.TableRow
import androidx.core.view.children
import ch.heig.soo_gui.xml_parser.element.Col
import ch.heig.soo_gui.xml_parser.element.Row
import com.heig.soo_gui.R
import kotlin.math.ceil

/**
 * Custom row.
 *
 */
class RowUI : TableRow, ElementUI {

    private var _row: Row? = null

    private lateinit var mLinearLayout: LinearLayout

    var row: Row?
        get() = _row
        set(value) {
            _row = value
            constructRow()
        }
    var maxCols = 12

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs)
    }

    @Suppress("UNUSED_PARAMETER")
    private fun init(attrs: AttributeSet?) {
        inflate(context, R.layout.view_row_ui, this)
        mLinearLayout = findViewById(R.id.view_row_ui_ll)
        constructRow()
    }

    /**
     * Construct the UI from the row model.
     * Request the new view at the end.
     *
     */
    private fun constructRow() {
        // clean
        this.removeAllViews()

        if (_row != null) {
            // adjust spanning
            val nbAuto = _row!!.cols.filter { col -> col.span is Col.Spanning.AUTO }.size
            val sumSpanning = _row!!.cols
                .filter { col -> col.span is Col.Spanning.FIXED }
                .sumOf { col -> (col.span as Col.Spanning.FIXED).nbCols + col.offset }

            for (col in _row!!.cols) {
                // adjust all the auto cols as fixed
                if (col.span is Col.Spanning.AUTO) {
                    val fixedSpan = ceil((maxCols - sumSpanning).toFloat() / nbAuto).toInt()
                    col.span = Col.Spanning.FIXED(fixedSpan)
                }

                val colView = ColUI(context)
                colView.col = col

                // add offset
                if (col.offset > 0) {
                    val offsetSpace = Space(context)
                    offsetSpace.layoutParams = LayoutParams(
                        0, // width
                        FrameLayout.LayoutParams.MATCH_PARENT, // height
                        col.offset.toFloat() //weight
                    )
                    this.addView(offsetSpace)
                }

                // add col
                this.addView(colView)
            }

            // add spacing at the end if not complete
            if (nbAuto == 0 && sumSpanning < maxCols) {
                val paddingSpace = Space(context)
                paddingSpace.layoutParams = LayoutParams(
                    0, // width
                    FrameLayout.LayoutParams.MATCH_PARENT, // height
                    (maxCols - sumSpanning).toFloat() //weight
                )
                this.addView(paddingSpace)
            }
        }// else

        invalidate()
        requestLayout()
    }// constructRow(...)

    /**
     * Add a view to the contained LinearLayout instead of the current TableRow.
     *
     * @param child The view to add.
     */
    override fun addView(child: View?) {
        mLinearLayout.addView(child)
    }

    /**
     * Remove the view from the LinearLayout instead of the current TableRow.
     *
     * @param view The view to remove.
     */
    override fun removeView(view: View?) {
        mLinearLayout.removeView(view)
    }

    /**
     * Remove the view at the index from the LinearLayout instead of the current TableRow.
     *
     * @param index Index to remove.
     */
    override fun removeViewAt(index: Int) {
        mLinearLayout.removeViewAt(index)
    }

    /**
     * Remove all views from the LinearLayout instead of the current TableRow.
     *
     */
    override fun removeAllViews() {
        mLinearLayout.removeAllViews()
    }

    override fun notifyUpdate(id: String) {
        if (_row?.id == id) {
            constructRow()
        } else if (_row != null) {
            for (view in mLinearLayout.children) {
                if (view is ElementUI) {
                    view.notifyUpdate(id)
                }
            }
        }

        visibility = GONE
        visibility = VISIBLE
    }
}