package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.pages.MobileEntityInterface
import ch.heig.soo_gui.pages.MobileEntityPageFragment
import ch.heig.soo_gui.utils.Utils.getCurrentNavigationFragment
import ch.heig.soo_gui.xml_parser.Event
import ch.heig.soo_gui.xml_parser.element.Dropdown
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import com.google.android.material.textfield.TextInputLayout
import com.heig.soo_gui.R

/**
 * Custom dropdown.
 *
 */
class DropdownUI : ConstraintLayout, ElementUI {

    private var _dropdown: Dropdown? = null
    private lateinit var mDropdownView: MaterialAutoCompleteTextView
    private lateinit var mTextInputLayout: TextInputLayout

    var dropdown: Dropdown?
        get() = _dropdown
        set(value) {
            _dropdown = value
            constructDropdown()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    @Suppress("UNUSED_PARAMETER")
    fun init(attrs: AttributeSet?, defStyle: Int) {
        inflate(context, R.layout.view_dropdown_ui, this)

        // get the components
        mDropdownView = findViewById(R.id.view_dropdown_ui_mactv)
        mTextInputLayout = findViewById(R.id.view_dropdown_ui_til)

        // set layout
        layoutParams = LinearLayout.LayoutParams(
            0, // width
            LinearLayout.LayoutParams.WRAP_CONTENT, // height
            1f
        )

        constructDropdown()
    }


    private fun constructDropdown() {
        if (_dropdown == null) {
            mTextInputLayout.visibility = GONE
            mDropdownView.setOnClickListener(null)
        } else {
            mTextInputLayout.visibility = VISIBLE

            // add the options
            val items = _dropdown!!.options.map { option -> option.content }
            val adapter = ArrayAdapter(context, R.layout.item_dropdown_ui, items)
            mDropdownView.setAdapter(adapter)

            // set the selected option
            if (_dropdown!!.selectedOption != null) {
                mDropdownView.setText(_dropdown!!.selectedOption!!.content, false)
            }

            // add onclick item. Send the value and not the content.
            val optionMap = _dropdown!!.options.map{option -> option.content to option }.toMap()
            val fragment = context.getCurrentNavigationFragment()

            mDropdownView.setOnItemClickListener { _, _, position, _ ->
                val selectedString = adapter.getItem(position)
                val selectedOption = optionMap[selectedString]
                _dropdown!!.selectedOption = selectedOption
                if (fragment is MobileEntityInterface) {
                    fragment.send(
                        MessageType.POST,
                        Event(
                            from = _dropdown!!.id,
                            action = Event.Action.VALUE_CHANGED(selectedOption!!.value)
                        )
                    )
                }
            }
        }// else

        invalidate()
        requestLayout()
    }// constructDropdown()

    override fun notifyUpdate(id: String) {
        if (_dropdown?.id == id) {
            constructDropdown()
            visibility = GONE
            visibility = VISIBLE
        }
    }
}