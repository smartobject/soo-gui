package ch.heig.soo_gui.widgets.elements

import android.app.ActionBar
import android.content.Context
import android.os.CountDownTimer
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.content.res.AppCompatResources
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.pages.MobileEntityInterface
import ch.heig.soo_gui.utils.Utils.getColors
import ch.heig.soo_gui.utils.Utils.getCurrentNavigationFragment
import ch.heig.soo_gui.utils.Utils.dpToPixel
import ch.heig.soo_gui.xml_parser.Event
import ch.heig.soo_gui.xml_parser.element.Button
import com.google.android.material.button.MaterialButton
import com.heig.soo_gui.R

/**
 * Custom lockable Button.
 *
 */
class ButtonUI : MaterialButton, ElementUI {
    private val TAG = "ButtonUI"

    private var _button: Button? = null
    private var startTime: Long = 0
    private var timer: CountDownTimer? = null

    private var locked: Boolean = false
        set(value) {
            field = value
            isSelected = value
            setLockIcon()
        }

    var button: Button?
        get() = _button
        set(value) {
            _button = value
            constructButton()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    @Suppress("UNUSED_PARAMETER")
    fun init(attrs: AttributeSet?, defStyle: Int) {
        // apply style which wil never change
        backgroundTintList = context.getColors(R.color.color_background_button_ui)
        iconTint = context.getColors(R.color.color_text_button_ui)
        strokeColor = context.getColors(R.color.color_stroke_button_ui)
        setTextColor(context.getColors(R.color.color_text_button_ui))
        strokeWidth = context.dpToPixel(1f).toInt()
        layoutParams = LinearLayout.LayoutParams(
            0,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            1f
        )

        // construct the button
        constructButton()
    }

    /**
     * Construct the UI from the button model.
     * Request the new view at the end.
     *
     */
    private fun constructButton() {
        if (_button == null) {
            @Suppress("ClickableViewAccessibility")
            setOnTouchListener(null)

            visibility = GONE
        } else {
            text = _button!!.text
            setLockIcon()

            @Suppress("ClickableViewAccessibility")
            setOnTouchListener { _, event ->
                val fragment = context.getCurrentNavigationFragment()
                val lockedPreviousState = locked

                // implement locking
                if (_button!!.lockable) {

                    // add action
                    when (val lockableAfter = _button!!.lockableAfter) {
                        Button.LockableAfter.ON_CLICK ->
                            if (event.action == MotionEvent.ACTION_DOWN) {
                                locked = !locked
                            }
                        Button.LockableAfter.ON_DOUBLE_CLICK ->
                            if (event.action == MotionEvent.ACTION_DOWN) {
                                if (locked) {
                                    // unlock
                                    locked = false
                                    startTime = 0
                                } else {
                                    // check if double click is below 1 second
                                    val endTime = System.currentTimeMillis()
                                    if (endTime - startTime < 1000) {
                                        // lock after double-click
                                        locked = true
                                    }
                                    startTime = endTime
                                }

                            }
                        is Button.LockableAfter.SECONDS ->
                            if (event.action == MotionEvent.ACTION_DOWN) {
                                if (locked) {
                                    // unlock
                                    locked = false
                                    timer = null
                                } else {
                                    // reset timer
                                    timer?.cancel()
                                    timer = object : CountDownTimer(
                                        (lockableAfter.duration * 1000).toLong(),
                                        1000
                                    ) {
                                        override fun onTick(millisUntilFinished: Long) {
                                            Log.i(
                                                TAG,
                                                "Time remaining until locking : $millisUntilFinished ms"
                                            )
                                        }

                                        override fun onFinish() {
                                            locked = true
                                        }
                                    }

                                    (timer as CountDownTimer).start()
                                }
                            } else if (event.action == MotionEvent.ACTION_UP) {
                                // on release, cancel timer
                                timer?.cancel()
                            }
                    }
                }

                isPressed = locked
                Log.d(TAG, "${_button!!.id} locked : $locked")

                // send event on click
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        // on press down, send event
                        if (fragment is MobileEntityInterface && !lockedPreviousState) {
                            fragment.send(
                                MessageType.POST,
                                Event(
                                    from = _button!!.id,
                                    action = Event.Action.CLICK_DOWN
                                )
                            )
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        // on press up, send event
                        if (fragment is MobileEntityInterface && !locked) {
                            fragment.send(
                                MessageType.POST,
                                Event(
                                    from = _button!!.id,
                                    action = Event.Action.CLICK_UP
                                )
                            )
                        }
                    }

                }

                // Returns false means that the action is not consumed by the touch, onClick receive it.
                // Returns true means that the action is consumed by touch, onClick doesn't receive it.
                return@setOnTouchListener false
            } // buttonView.setOnTouchListener

            visibility = VISIBLE
        }// else

        invalidate()
        requestLayout()
    }// constructButton(...)

    /**
     * Set the locking icon.
     * If the button is not lockable then there are no icon, otherwise if the icon is an open lock
     * if the button is unlocked and a close lock if the button is locked.
     *
     */
    private fun setLockIcon() {
        icon = if (_button != null && _button!!.lockable) {
            if (locked) {
                AppCompatResources.getDrawable(context, android.R.drawable.ic_secure)
            } else {
                AppCompatResources.getDrawable(context, android.R.drawable.ic_partial_secure)
            }
        } else {
            null
        }
    }

    override fun notifyUpdate(id: String) {
        if (_button?.id == id) {
            constructButton()
            visibility = GONE
            visibility = VISIBLE
        }
    }
}