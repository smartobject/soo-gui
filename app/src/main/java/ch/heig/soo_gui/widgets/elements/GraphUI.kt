package ch.heig.soo_gui.widgets.elements

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.text.format.DateFormat
import android.util.AttributeSet
import android.util.Log
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import ch.heig.soo_gui.utils.Utils.getColorFromAttr
import ch.heig.soo_gui.xml_parser.element.Axis
import ch.heig.soo_gui.xml_parser.element.Graph
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.android.material.textview.MaterialTextView
import com.heig.soo_gui.R
import ir.androidexception.datatable.DataTable
import ir.androidexception.datatable.model.DataTableHeader
import ir.androidexception.datatable.model.DataTableRow
import java.text.SimpleDateFormat

/**
 * Custom graph.
 * The graph can be either a line graph or a table
 *
 */
class GraphUI : ConstraintLayout, ElementUI {

    private val TAG = "GraphUI"
    private lateinit var mLineChartConstraintLayout: ConstraintLayout
    private lateinit var mLineChartView: LineChart
    private lateinit var mXLineChartLabel: MaterialTextView
    private lateinit var mYLineChartLabel: MaterialTextView
    private lateinit var mTableConstraintLayout: ConstraintLayout
    private lateinit var mTableDataTable: DataTable
    private var _graph: Graph? = null

    var graph: Graph?
        get() = _graph
        set(value) {
            _graph = value
            constructGraph()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    @Suppress("UNUSED_PARAMETER")
    fun init(attrs: AttributeSet?, defStyle: Int) {
        inflate(context, R.layout.view_graph_ui, this)

        // get components
        mLineChartConstraintLayout = findViewById(R.id.view_graph_ui_line_chart_cl)
        mLineChartView = findViewById(R.id.view_graph_ui_line_chart)
        mXLineChartLabel = findViewById(R.id.view_graph_ui_x_axis_label)
        mYLineChartLabel = findViewById(R.id.view_graph_ui_y_axis_label)
        mTableConstraintLayout = findViewById(R.id.view_graph_ui_table_cl)
        mTableDataTable = findViewById(R.id.view_graph_ui_table)

        // set layout
        layoutParams = LinearLayout.LayoutParams(
            0, // width
            LinearLayout.LayoutParams.WRAP_CONTENT, // height
            1f
        )
        constructGraph()
    }

    /**
     * Construct the UI from the graph model.
     * Request the new view at the end.
     *
     */
    private fun constructGraph() {
        if (_graph == null) {
            mLineChartConstraintLayout.visibility = GONE
            mTableConstraintLayout.visibility = GONE
        } else {
            when (_graph!!.type) {
                Graph.Type.LINE -> createLineChart()
                Graph.Type.TABLE -> createTable()
            }
        }

        invalidate()
        requestLayout()
    }

    /**
     * Create a line chart.
     *
     */
    @SuppressLint("SimpleDateFormat")
    private fun createLineChart() {
        mLineChartConstraintLayout.visibility = VISIBLE

        // chart background color depend on the theme background color
        mLineChartView.setBackgroundColor(android.R.attr.colorBackground)

        // disable drawing background grid
        mLineChartView.setDrawGridBackground(false)

        // enable axis and set color
        mLineChartView.axisLeft.isEnabled = true
        mLineChartView.axisRight.isEnabled = true
        mLineChartView.xAxis.position = XAxis.XAxisPosition.BOTTOM

        // set color theme
        val textColor = context.getColorFromAttr(R.attr.colorOnBackground)
        mLineChartView.axisLeft.textColor = textColor
        mLineChartView.axisRight.textColor = textColor
        mLineChartView.xAxis.textColor = textColor
        mLineChartView.axisLeft.axisLineColor = textColor
        mLineChartView.axisRight.axisLineColor = textColor
        mLineChartView.xAxis.axisLineColor = textColor
        mLineChartView.axisLeft.gridColor = textColor
        mLineChartView.axisRight.gridColor = textColor
        mLineChartView.xAxis.gridColor = textColor
        mLineChartView.legend.textColor = textColor

        // enable scaling and dragging
        mLineChartView.isDragEnabled = true
        mLineChartView.setScaleEnabled(true)

        // force pinch zoom along both axis
        mLineChartView.setPinchZoom(true)

        // --- SET DATA ---
        val dataSets = ArrayList<ILineDataSet>()

        // enable/disable series names
        mLineChartView.legend.isEnabled = _graph!!.axes.displaySeriesName

        // disable description => not used
        mLineChartView.description.isEnabled = false

        // set axis formatter
        val stringArray: ArrayList<String> = ArrayList()
        when (val type = _graph!!.axes.axes[0].type) {
            is Axis.Type.DATETIME -> mLineChartView.xAxis.valueFormatter =
                object : ValueFormatter() {
                    override fun getAxisLabel(value: Float, axis: AxisBase?): String {
                        return DateFormat.format(type.format, value.toLong()).toString()
                    }
                }
            Axis.Type.NUMBER -> mLineChartView.xAxis.valueFormatter = object : ValueFormatter() {}
            Axis.Type.STRING -> mLineChartView.xAxis.valueFormatter =
                IndexAxisValueFormatter(stringArray)

        }

        when (val type = _graph!!.axes.axes[1].type) {
            is Axis.Type.DATETIME -> mLineChartView.axisLeft.valueFormatter =
                object : ValueFormatter() {
                    override fun getAxisLabel(value: Float, axis: AxisBase?): String {
                        return DateFormat.format(type.format, value.toLong()).toString()
                    }
                }
            Axis.Type.NUMBER -> mLineChartView.axisLeft.valueFormatter =
                object : ValueFormatter() {}
            Axis.Type.STRING -> mLineChartView.axisLeft.valueFormatter =
                IndexAxisValueFormatter(stringArray)

        }
        mLineChartView.axisRight.valueFormatter = mLineChartView.axisLeft.valueFormatter


        val axes = _graph!!.axes.axes

        // get the axis label if exists
        mXLineChartLabel.text = axes[0].text
        mYLineChartLabel.text = axes[1].text

        // set the series
        for ((idx, series) in _graph!!.series.withIndex()) {
            val values = ArrayList<Entry>()

            // add points
            for (point in series.points) {

                @Suppress("RE")
                try {

                    val x: Float = when (val type = _graph!!.axes.axes[0].type) {
                        is Axis.Type.DATETIME -> SimpleDateFormat(
                            type.format ?: ""
                        ).parse(point.items[0].value)?.time?.toFloat()
                            ?: throw NullPointerException("Parameter x is not a valid date")
                        Axis.Type.NUMBER -> point.items[0].value.toFloat()
                        Axis.Type.STRING -> {
                            stringArray.add(point.items[0].value)
                            stringArray.size.toFloat()
                        }
                    }

                    val y: Float = when (val type = _graph!!.axes.axes[1].type) {
                        is Axis.Type.DATETIME -> SimpleDateFormat(
                            type.format ?: ""
                        ).parse(point.items[1].value)?.time?.toFloat()
                            ?: throw NullPointerException("Parameter y is not a valid date")
                        Axis.Type.NUMBER -> point.items[1].value.toFloat()
                        Axis.Type.STRING -> {
                            stringArray.add(point.items[1].value)
                            stringArray.size.toFloat()
                        }
                    }

                    values.add(Entry(x, y))
                } catch (e: Exception) {
                    Log.e(TAG, "Point is not a number ($point)", e)
                }
            }

            // set width of line
            val dataSet = LineDataSet(values, series.name)
            dataSet.lineWidth = 3f
            dataSet.circleRadius = 4f
            dataSet.mode = LineDataSet.Mode.CUBIC_BEZIER

            // set color of line and circle
            val colors = resources.getIntArray(R.array.color_line_graph_ui)
            val color = colors[idx % colors.size]
            dataSet.color = color
            dataSet.setCircleColor(color)
            dataSet.valueTextColor = context.getColorFromAttr(R.attr.colorOnBackground)
            dataSets.add(dataSet)
        }

        val data = LineData(dataSets)
        mLineChartView.data = data
    }

    /**
     * Create a sortable table.
     *
     */
    private fun createTable() {
        mTableConstraintLayout.visibility = VISIBLE

        // add theme style
        mTableDataTable.setBackgroundColor(android.R.attr.colorBackground)
        mTableDataTable.headerBackgroundColor =
            context.getColorFromAttr(android.R.attr.colorBackground)
        mTableDataTable.rowBackgroundColor =
            context.getColorFromAttr(android.R.attr.colorBackground)
        mTableDataTable.headerTextColor = context.getColorFromAttr(R.attr.colorOnBackground)
        mTableDataTable.rowTextColor = context.getColorFromAttr(R.attr.colorOnBackground)

        // create header
        val headerBuilder = DataTableHeader.Builder()

        if (_graph!!.axes.displaySeriesName) {
            headerBuilder.item(context.getString(R.string.series_header_table), 1)
        }

        for (axis in _graph!!.axes.axes) {
            headerBuilder.item(axis.text, 1)
        }

        // create rows
        val rows = ArrayList<DataTableRow>()
        for (series in _graph!!.series) {
            for (point in series.points) {
                val dataTableRowBuilder = DataTableRow.Builder()
                if (_graph!!.axes.displaySeriesName) {
                    dataTableRowBuilder.value(series.name)
                }

                for (item in point.items) {
                    dataTableRowBuilder.value(item.value)
                }

                rows.add(dataTableRowBuilder.build())
            }
        }

        // set table
        mTableDataTable.header = headerBuilder.build()
        mTableDataTable.rows = rows
        mTableDataTable.inflate(context)
        mTableDataTable.requestLayout()
    }

    override fun notifyUpdate(id: String) {
        if (_graph?.id == id || _graph?.series?.any { series -> series.id == id } == true) {
            constructGraph()
            visibility = GONE
            visibility = VISIBLE
        }
    }
}