package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.pages.MobileEntityInterface
import ch.heig.soo_gui.utils.Utils.getCurrentNavigationFragment
import ch.heig.soo_gui.xml_parser.Event
import ch.heig.soo_gui.xml_parser.element.Slider
import com.google.android.material.card.MaterialCardView
import com.heig.soo_gui.R
import com.google.android.material.slider.Slider as MaterialSlider

/**
 * Custom slider.
 *
 */
class SliderUI : ConstraintLayout, ElementUI {
    private val TAG: String = "SliderUI"

    private var _slider: Slider? = null
    private lateinit var mSliderView: MaterialSlider
    private lateinit var mCardView: MaterialCardView

    var slider: Slider?
        get() = _slider
        set(value) {
            _slider = value
            constructSlider()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    @Suppress("UNUSED_PARAMETER")
    fun init(attrs: AttributeSet?, defStyle: Int) {
        inflate(context, R.layout.view_slider_ui, this)
        mSliderView = findViewById(R.id.view_slider_ui_slider)
        mCardView = findViewById(R.id.view_slider_ui_cv)

        // set layout
        layoutParams = LinearLayout.LayoutParams(
            0, // width
            LinearLayout.LayoutParams.WRAP_CONTENT, // height
            1f
        )

        constructSlider()
    }

    /**
     * Construct the UI from the slider model.
     * Request the new view at the end.
     *
     */
    private fun constructSlider() {
        mSliderView.clearOnChangeListeners()

        @Suppress("ClickableViewAccessibility")
        mSliderView.setOnTouchListener(null)

        if (_slider == null) {
            mSliderView.visibility = GONE
        } else {
            mSliderView.visibility = VISIBLE

            // set the values
            mSliderView.valueFrom = _slider!!.min.toFloat()
            mSliderView.valueTo = _slider!!.max.toFloat()
            val currentValue = _slider!!.value.toFloat()
            mSliderView.value = when {
                currentValue < mSliderView.valueFrom -> {
                    mSliderView.valueFrom
                }
                currentValue > mSliderView.valueTo -> {
                    mSliderView.valueTo
                }
                else -> {
                    currentValue
                }
            }
            mSliderView.stepSize = _slider!!.step.toFloat()

            // vertical slider
            if (_slider!!.orientation == Slider.Orientation.VERTICAL) {
                mSliderView.rotation = 270f
                mSliderView.layoutParams.width = 400
                mCardView.minimumHeight = 400
            } else {
                mSliderView.rotation = 0f
                mSliderView.layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT
                mCardView.minimumHeight = 0
            }

            // action on slider moved
            val fragment = context.getCurrentNavigationFragment()
            mSliderView.addOnChangeListener { _, value, _ ->
                if (fragment is MobileEntityInterface) {
                    fragment.send(
                        MessageType.POST,
                        Event(
                            from = _slider!!.id,
                            action = Event.Action.VALUE_CHANGED(value.toString())
                        )
                    )
                }
            }

            // to prevent scrollview to activate if slider down
            @Suppress("ClickableViewAccessibility")
            mSliderView.setOnTouchListener { _, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN ->
                        if (fragment is MobileEntityInterface) {
                            fragment.lockScrolling(true)
                            Log.d(TAG, "scrollview locked")
                        }
                    MotionEvent.ACTION_UP ->
                        if (fragment is MobileEntityInterface) {
                            fragment.lockScrolling(false)
                            Log.d(TAG, "scrollview unlocked")
                        }
                }
                return@setOnTouchListener false
            }
        }

        invalidate()
        requestLayout()
    }// constructSlider(...)

    override fun notifyUpdate(id: String) {
        if (_slider?.id == id) {
            constructSlider()
            visibility = GONE
            visibility = VISIBLE
        }
    }
}