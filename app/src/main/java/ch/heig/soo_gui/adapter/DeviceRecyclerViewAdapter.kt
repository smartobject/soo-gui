@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ch.heig.soo_gui.bluetooth.Device
import com.heig.soo_gui.R

data class DeviceRecyclerViewAdapter(
    val items: ArrayList<Device>,
    val onConnection: (name: String, address: String) -> Unit
) :
    RecyclerView.Adapter<DeviceRecyclerViewAdapter.DeviceViewHolder>() {

    private val TAG = "DeviceRecyclerViewAdapter"

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DeviceRecyclerViewAdapter.DeviceViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_device, parent, false)
        return DeviceViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: DeviceRecyclerViewAdapter.DeviceViewHolder,
        position: Int
    ) {
        val item = items[position]
        holder.nameTextView.text = item.name
        holder.addressTextView.text = item.address
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class DeviceViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView = view.findViewById(R.id.item_device_name_label)
        val addressTextView: TextView = view.findViewById(R.id.item_device_address_label)
        private val connectButton: Button = view.findViewById(R.id.item_device_connect_btn)

        init {
            connectButton.setOnClickListener {
                Log.d(TAG, "Trying to connect to ${nameTextView.text} at ${addressTextView.text}")
                onConnection(nameTextView.text.toString(), addressTextView.text.toString())
            }
        }
    }
}
