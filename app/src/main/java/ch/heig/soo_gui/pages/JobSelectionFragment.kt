package ch.heig.soo_gui.pages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ch.heig.soo_gui.KEY_MOBILE_ENTITIES_ID
import ch.heig.soo_gui.xml_parser.MobileEntity
import com.heig.soo_gui.R

class JobSelectionFragment : Fragment() {

    private val TAG = "JobSelectionFragment"

    private var mobileEntities : Array<MobileEntity> = emptyArray()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_job_selection_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mobileEntities = arguments?.get(KEY_MOBILE_ENTITIES_ID) as Array<MobileEntity>? ?: emptyArray()

        val buttonConnectToME: Button = view.findViewById(R.id.goto_me_connect_btn)
        buttonConnectToME.setOnClickListener(this@JobSelectionFragment::connectToME)
        val buttonSendME: Button = view.findViewById(R.id.goto_send_me_btn)
        buttonSendME.setOnClickListener(this@JobSelectionFragment::sendME)
    }

    @Suppress("UNUSED_PARAMETER")
    fun connectToME(view: View) {
        findNavController().navigate(JobSelectionFragmentDirections.actionJobSelectionFragmentToMobileEntitiesPageFragment(mobileEntities))
    }

    @Suppress("UNUSED_PARAMETER")
    fun sendME(view: View) {
        findNavController().navigate(JobSelectionFragmentDirections.actionJobSelectionFragmentToSendMEPageFragment())
    }
}