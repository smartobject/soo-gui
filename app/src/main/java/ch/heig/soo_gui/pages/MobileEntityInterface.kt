package ch.heig.soo_gui.pages

import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.xml_parser.Event

interface MobileEntityInterface {

    /**
     * Send a message to the Mobile Entity.
     *
     * @param messageType Type of the message.
     * @param event The event with the possible payload.
     */
    fun send(messageType: MessageType, event: Event)

    /**
     * Lock scrolling.
     * Useful for the sliders.
     *
     * @param locked True if the scrolling need to be locked, false otherwise.
     */
    fun lockScrolling(locked: Boolean)
}