@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.pages

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import ch.heig.soo_gui.KEY_MODEL_ID
import ch.heig.soo_gui.KEY_SLOTID_ID
import ch.heig.soo_gui.KEY_SPID_ID
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.viewmodel.BluetoothDevicesViewModel
import ch.heig.soo_gui.viewmodel.ModelViewModel
import ch.heig.soo_gui.widgets.LockableScrollView
import ch.heig.soo_gui.widgets.elements.LayoutUI
import ch.heig.soo_gui.xml_parser.*
import ch.heig.soo_gui.xml_parser.element.Layout
import com.google.android.material.textview.MaterialTextView
import com.heig.soo_gui.R


class MobileEntityPageFragment : Fragment(), MobileEntityInterface {

//    companion object {
//        fun newInstance() = MobileEntityPageFragment()
//    }

    private val TAG = "MobileEntityPageFragment"

    private val mBluetoothDevicesViewModel: BluetoothDevicesViewModel by viewModels({ requireParentFragment() })
    private lateinit var mModelViewModel: ModelViewModel
    private lateinit var mConstraintLayout: ConstraintLayout
    private lateinit var mLayoutUI: LayoutUI
    private lateinit var mLockableScrollView: LockableScrollView
    private lateinit var mNameMaterialTextView: MaterialTextView
    private lateinit var mDescriptionMaterialTextView: MaterialTextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mobile_entity_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // get the ui elements
        mConstraintLayout = view.findViewById(R.id.fragment_mobile_entity_page_main_cl)
        mLayoutUI = view.findViewById(R.id.fragment_mobile_entity_page_layout_ui)
        mLockableScrollView = view.findViewById(R.id.fragment_mobile_entity_page_sv)
        mNameMaterialTextView = view.findViewById(R.id.fragment_mobile_entity_page_name_label)
        mDescriptionMaterialTextView =
            view.findViewById(R.id.fragment_mobile_entity_page_description_label)

        // get the model
        val model: Model = arguments?.get(KEY_MODEL_ID) as Model? ?: Model(layout = Layout())
        mModelViewModel = ViewModelProvider(this).get(ModelViewModel::class.java)
        mModelViewModel.modelLiveData.observe(viewLifecycleOwner, { newModel ->
            // update the UI
            mNameMaterialTextView.text = newModel.name ?: ""
            mDescriptionMaterialTextView.text = newModel.description ?: ""

            mLayoutUI.layout = newModel.layout
        })
        mModelViewModel.resetModel(model)

        // get the spid
        mModelViewModel.spid = arguments?.getString(KEY_SPID_ID)
        mModelViewModel.slotID = arguments?.getInt(KEY_SLOTID_ID)

        Log.i(TAG, "SLOTID ${mModelViewModel.slotID}")

        mBluetoothDevicesViewModel.onConnectionLost = Handler(Looper.getMainLooper()) {
            try {
                Toast.makeText(context, R.string.error_bluetooth_connection_lost, Toast.LENGTH_LONG)
                    .show()
                Handler(Looper.getMainLooper()).postDelayed({
                    try {
                        findNavController().navigate(
                            MobileEntityPageFragmentDirections.actionMobileEntityPageFragmentToHomePageFragment()
                        )
                    } catch (e: Exception) {
                        Log.e(TAG, "error", e)
                    }
                }, 3000)
            } catch (e: Exception) {
                Log.e(TAG, "error", e)
            }
            return@Handler true
        }

        // change the messageHandler
        mBluetoothDevicesViewModel.messageHandler = Handler(Looper.getMainLooper()) { message ->
            return@Handler when (message.what) {
                MessageType.SEND.ordinal -> {
                    Log.i(TAG, "Message received : ${String(message.obj as ByteArray)}")

                    try {
                        val parser = XmlParser()
                        val inputStream = (message.obj as ByteArray).inputStream()

                        // parse the message by its type
                        val receivedMessage = parser.parse(inputStream)
                        when (receivedMessage.type) {
                            XmlParser.ContentType.MOBILE_ENTITIES -> throw IllegalStateException("Cannot receive a <mobile-entities> message anymore.")
                            XmlParser.ContentType.MODEL -> {
                                val newModel = receivedMessage .content as Model
                                mModelViewModel.resetModel(newModel)
                            }
                            XmlParser.ContentType.MESSAGES -> {
                                @Suppress("UNCHECKED_CAST")
                                val messages = receivedMessage.content as List<Message>
                                for (messageIter in messages) {
                                    mModelViewModel.update(messageIter)
                                    mLayoutUI.notifyUpdate(messageIter.to)
                                }
                            }
                            XmlParser.ContentType.UNKNOWN -> throw IllegalStateException("Unknown message.")
                        }
                    } catch (e: Exception) {
                        Log.e(TAG, "Error", e)
                        Toast.makeText(
                            requireContext(),
                            resources.getString(R.string.try_model_again_toast),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    true
                }
                else -> false
            }
        } // mBluetoothDevicesViewModel.messageHandler
    }

    override fun send(messageType: MessageType, event: Event) {
        val payload = XmlWriter.writeEvents(listOf(event))
        mBluetoothDevicesViewModel.send(
            messageType = messageType,
            spid = mModelViewModel.spid,
            payload = payload,
            slotID = mModelViewModel.slotID
        )
        Log.i(TAG, "message send to ${mModelViewModel.slotID} : $payload")
    }

    override fun lockScrolling(locked: Boolean) {
        mLockableScrollView.setScrollingEnabled(!locked)
    }
}