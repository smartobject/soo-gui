
@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.pages

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import ch.heig.soo_gui.network.*
import ch.heig.soo_gui.viewmodel.BluetoothDevicesViewModel
import com.heig.soo_gui.R
import java.io.ByteArrayOutputStream


class SendMEPageFragment : Fragment() {

//    companion object {
//        fun newInstance() = MobileEntityPageFragment()
//    }

    private val TAG = "SendMEPageFragment"

    private val mBluetoothDevicesViewModel: BluetoothDevicesViewModel by viewModels({ requireParentFragment() })
    private lateinit var mBluetoothAdapter: BluetoothAdapter

    private lateinit var uriText: TextView


    private var meURI: Uri? = null

    // Used to react to the file dialog choice
    private val askFile = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if (it.resultCode == Activity.RESULT_OK) {
            meURI = it.data?.data!!
            uriText.text = meURI?.path
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_send_me_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val buttonConnectToME: Button = view.findViewById(R.id.select_me_btn)
        buttonConnectToME.setOnClickListener(this@SendMEPageFragment::selectMEFile)
        val buttonSendME: Button = view.findViewById(R.id.sendMEbtn)
        buttonSendME.setOnClickListener(this@SendMEPageFragment::sendME)

        uriText = view.findViewById(R.id.me_selected)

        mBluetoothDevicesViewModel.messageHandler = Handler(Looper.getMainLooper()) { message ->
            return@Handler when (message.what) {
                MessageType.SEND.ordinal -> {
                    Log.i(TAG, "Message received : ${String(message.obj as ByteArray)}")


                    true
                }
                else -> false
            }
        }
    }

    private fun performFileSearch() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            type = "*/*"
        }
        askFile.launch(intent)
    }

    @Suppress("UNUSED_PARAMETER")
    fun selectMEFile(view: View) {
        performFileSearch()
    }

    public fun convertIntToByteArray(value: Int): ByteArray {
        var result = ByteArray(Int.SIZE_BYTES)

        val mask = 0x00FF
        var n = value

        for (i in 0 until result.size) {
            result[i] = n.and(mask).toByte()
            n = n.shr(Byte.SIZE_BITS)
        }

        return result
    }

    private fun sendME (view: View) {
        var numBytes: Int
        val buffer: ByteArrayOutputStream = ByteArrayOutputStream()
        val readFileBuffer = ByteArray(8192)
        val context = requireContext()
        var contentResolver: ContentResolver = context.getContentResolver()
        var fileInputStream = contentResolver.openInputStream(meURI!!)
        do {
            numBytes = fileInputStream!!.read(readFileBuffer, 0, readFileBuffer.size)
            Log.i("BUFFER", "Read $numBytes bytes")
            if (numBytes != -1)
                buffer.write(readFileBuffer, 0, numBytes)
        } while (numBytes != -1 && numBytes == readFileBuffer.size)

        buffer.flush()
        val size: Int = buffer.size()
        val sizeBuf = convertIntToByteArray(size)

        mBluetoothDevicesViewModel.send_plain(sizeBuf, ME_SIZE_PKT, -1)

        mBluetoothDevicesViewModel.send_plain(buffer.toByteArray(), ME_DATA_PKT, -1)
    }
}