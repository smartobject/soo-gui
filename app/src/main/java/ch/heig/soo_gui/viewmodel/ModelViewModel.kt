package ch.heig.soo_gui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.Model
import ch.heig.soo_gui.xml_parser.element.Element
import ch.heig.soo_gui.xml_parser.element.Layout

class ModelViewModel : ViewModel() {
    private var idsMap: Map<String, Element>
    val modelLiveData = MutableLiveData<Model>()
    private var model: Model = Model(layout = Layout())
    var spid: String? = null
    var slotID: Int? = 0

    init {
        model = Model(layout = Layout())
        idsMap = emptyMap()
        updateModelLiveData()
    }

    /**
     * Reset the [ch.heig.soo_gui.xml_parser.Model] stored in the ViewModel.
     * In opposition to the update, it will replace all the layout.
     *
     * @param model Model to replace.
     */
    fun resetModel(model: Model) {
        this.model = model
        idsMap = model.layout
            .toList()
            .filter { e -> !e.id.isNullOrBlank() }
            .map { e -> e.id!! to e }
            .toMap()
        updateModelLiveData()
    }

    /**
     * Update the content inside an [ch.heig.soo_gui.xml_parser.element.Element].
     * The updated element is referred by its id.
     *
     * @param id Id of the element to update the content.
     * @param elements New elements to replace inside.
     */
    fun update(message: Message) {
        idsMap[message.to]?.update(message.content, message.type)
        idsMap = model.layout
            .toList()
            .filter { e -> !e.id.isNullOrBlank() }
            .map { e -> e.id!! to e }
            .toMap()
    }

    /**
     * Force the update of the LiveData for the model.
     *
     */
    private fun updateModelLiveData() {
        modelLiveData.value = this.model
    }

}