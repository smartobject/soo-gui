package ch.heig.soo_gui.viewmodel

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.os.Handler
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ch.heig.soo_gui.bluetooth.BLUETOOTH_UUID
import ch.heig.soo_gui.bluetooth.Device
import ch.heig.soo_gui.network.ClientThread
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.network.NetworkPlainHandler
import ch.heig.soo_gui.network.NetworkVUIHandler
import ch.heig.soo_gui.utils.Utils.unaccented
import java.io.IOException
import java.util.concurrent.atomic.AtomicReference

class BluetoothDevicesViewModel : ViewModel() {
    private val TAG = "BluetoothDevicesViewModel"
    private val bluetoothDevices = ArrayList<BluetoothDevice>()
    private val devices = ArrayList<Device>()
    private var bluetoothSocket: BluetoothSocket? = null
    private var clientThread: ClientThread? = null
    val devicesLiveData = MutableLiveData<ArrayList<Device>>()
    val filterLiveData = MutableLiveData<String>()
    private val filteredDevices = ArrayList<Device>()
    val selectedBluetoothDevice = MutableLiveData<BluetoothDevice?>()
    var messageHandler: Handler? = null
        set(value) {
            field = value
            if (value != null && bluetoothSocket != null) {
                clientThread?.networkHandler?.inputHandler?.set(messageHandler!!)
            }
        }
    var onConnectionLost: Handler? = null

    init {
        filterLiveData.value = ""
        filterLiveData.observeForever { updateDevicesLiveData() }
        updateDevicesLiveData()

        selectedBluetoothDevice.observeForever { device ->
            if (device == null) {
                clientThread?.interrupt()
                clientThread = null
                try {
                    bluetoothSocket?.inputStream?.close()
                    bluetoothSocket?.outputStream?.close()
                } catch (e: Exception) {
                    Log.e(TAG, "exception", e)
                }
                bluetoothSocket = null
            } else {
                bluetoothSocket = device.createRfcommSocketToServiceRecord(BLUETOOTH_UUID)
            }
        }
    }

    /**
     * Add a new device to the list.
     *
     * @param device New Bluetooth device to add.
     */
    fun addDevice(device: BluetoothDevice?) {
        // There is an assert on name not being null but it can be null.
        try {
            if (device != null
                && !bluetoothDevices.contains(device)
//                && device.name.lowercase().contains("soo")
            ) {
                devices.add(Device.from(device))
                bluetoothDevices.add(device)
                updateDevicesLiveData()
            }
        } catch (e: Exception) {
        }
    }

    /**
     * Clear al the devices.
     *
     */
    fun clearDevices() {
        bluetoothDevices.clear()
        devices.clear()
        updateDevicesLiveData()
    }

    /**
     * Update the devices livedata.
     *
     */
    private fun updateDevicesLiveData() {
        filteredDevices.clear()
        filteredDevices.addAll(
            devices.filter { device ->
                filterLiveData.value?.let {
                    device.name.contains(it, ignoreCase = true)
                } == true
                        || filterLiveData.value?.let {
                    device.name.unaccented().contains(it, ignoreCase = true)
                } == true
            }
//           .sortedBy { d -> d.name }
        )
        devicesLiveData.value = filteredDevices
    }

    /**
     * Get the Bluetooth device by its address.
     *
     * @param address Address of the Bluetooth device.
     * @return The BluetoothDevice in question.
     */
    fun getBluetoothDevice(address: String): BluetoothDevice? {
        return bluetoothDevices.firstOrNull { it.address == address }
    }

    /**
     * Establish the connection to the bluetooth socket.
     *
     * @return True if connected or False otherwise.
     */
    @Throws(IOException::class, NullPointerException::class)
    fun connect(): Boolean {
        if (messageHandler == null) {
            throw NullPointerException("messageHandler is null.")
        }

        bluetoothSocket?.connect()

        // create and launch thread if not null
        clientThread = if (bluetoothSocket != null) {
            ClientThread(
                /*NetworkPlainHandler(
                    inputStream = bluetoothSocket!!.inputStream,
                    outputStream = bluetoothSocket!!.outputStream,
                    inputHandler = AtomicReference(messageHandler!!)
                ),*/
                NetworkVUIHandler(
                    inputStream = bluetoothSocket!!.inputStream,
                    outputStream = bluetoothSocket!!.outputStream,
                    inputHandler = AtomicReference(messageHandler!!)
                ),
                onExceptionHandler = {
                    selectedBluetoothDevice.postValue(null)
                    onConnectionLost?.obtainMessage(0)?.sendToTarget()
                }
            )
        } else {
            null
        }
        clientThread?.start()

        return bluetoothSocket != null && bluetoothSocket!!.isConnected
    }

    fun send(messageType: MessageType, spid: String? = null, payload: String? = null, slotID: Int? = 0): Boolean {
        return if (clientThread != null) {
            clientThread!!.send(messageType, spid, payload, slotID)
            true
        } else {
            false
        }
    }

    fun send_plain(payload: ByteArray, type: Int, slotID: Int): Boolean {
        return if (clientThread != null) {
            clientThread!!.send_plain(payload, type, slotID)
            true
        } else {
            false
        }
    }
}