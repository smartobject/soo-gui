package ch.heig.soo_gui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ch.heig.soo_gui.utils.Utils.unaccented
import ch.heig.soo_gui.xml_parser.MobileEntity

class MobileEntitiesViewModel : ViewModel() {
    private val mobileEntities = ArrayList<MobileEntity>()
    val mobileEntityLiveData = MutableLiveData<ArrayList<MobileEntity>>()
    val filterLiveData = MutableLiveData<String>()
    private val filteredMobileEntities = ArrayList<MobileEntity>()

    init {
        filterLiveData.value = ""
        filterLiveData.observeForever { updateMobileEntitiesLiveData() }
        updateMobileEntitiesLiveData()
    }

    /**
     * Add a collection of mobile entities.
     *
     * @param mobileEntities the collection to add.
     */
    fun addMobileEntities(mobileEntities: Collection<MobileEntity>) {
        for (me in mobileEntities) {
            addMobileEntity(me)
        }
    }

    /**
     * Add a single mobile entity.
     *
     * @param mobileEntity The new mobile entity to add.
     */
    fun addMobileEntity(mobileEntity: MobileEntity) {
        if (!mobileEntities.contains(mobileEntity)) {
            mobileEntities.add(mobileEntity)
            updateMobileEntitiesLiveData()
        }
    }

    /**
     * Clear all the mobile entities.
     *
     */
    fun clearMobileEntities() {
        mobileEntities.clear()
        updateMobileEntitiesLiveData()
    }

    /**
     * Update the mobile entities livedata with the filter.
     *
     */
    private fun updateMobileEntitiesLiveData() {
        filteredMobileEntities.clear()
        filteredMobileEntities.addAll(
            mobileEntities.filter { mobileEntity ->
                filterLiveData.value?.let {
                    mobileEntity.name.contains(
                        it,
                        ignoreCase = true
                    )
                    || mobileEntity.description?.contains(it, ignoreCase = true) == true
                    || mobileEntity.name.unaccented().contains(
                        it,
                        ignoreCase = true
                    )
                    || mobileEntity.description?.unaccented()
                        ?.contains(it, ignoreCase = true) == true
                } == true
            }.sortedBy { d -> d.name }
        )
        mobileEntityLiveData.value = filteredMobileEntities
    }
}