Welcome to Smart Object Oriented Graphic User Interface (SOO-GUI) technology
*************************************************

This is an Android app to display information from components of
the [SOO framework](https://gitlab.com/smartobject/soo).

It uses the SOOUIP protocol as describe in
this [RFCs](https://gitlab.com/smartobject/soo-gui/-/tree/master/doc).

The application implements
the [0001-RFC](https://gitlab.com/smartobject/soo-gui/-/blob/master/doc/0001-rfc.md)
and [0002-RFC](https://gitlab.com/smartobject/soo-gui/-/blob/master/doc/0002-rfc.md).
